/**
 * @file
 * Contains the javascript api implementation for the vfc file field types.
 */

(function ($) {
  Drupal.behaviors.vfc_file = {
    attach: function (context, settings) {

      jQuery.extend(Drupal.vfc.fieldapi, {

        // Normal file uploads.
        file: {

          /**
           * Implements vfc callback checkConfiguration().
           */
          checkConfiguration: function(field, definition, $DOMelem, allowedExtensions) {
            var errors = [];

            if (allowedExtensions === undefined) {
              allowedExtensions = [];
            }

            if (!field.label.length) {
              field.label = '&lt;no field label&gt;';
            }

            if (!field.settings.types.length) {
              // No file extensions entered, this is always an error...
              errors.push(Drupal.t(
                'Please state allowed file extensions on field "!fieldlabel"',
                {'!fieldlabel': field.label}
              ));
            } else {
              // Something was entered, let's examine it...

              // A wildcard was entered, make sure it's the only "extension" entered...
              if (field.settings.types.trim().indexOf('.*') !== -1) {
                field.settings.types = '.*';
              }

              // Prepare the extensions settings string in the correct format...
              field.settings.types = field.settings.types.trim().replace(/[,;+-]+/, ' ').replace(/\ +/, ' ');

              // Adjust the contents of the settings field...
              $('input[data-type="types"]', $DOMelem).val(field.settings.types);

              var types = field.settings.types.split(' ');
              for (var index in types) {
                var type = types[index];
                // Verify the extension format...
                if (!type.match(/\.(?:\*|[a-z0-9]+)/i)) {
                  errors.push(Drupal.t(
                    '"!type" is not a correct file extension (e.g. ".pdf") on field "!fieldlabel"',
                    {
                      '!type': type,
                      '!fieldlabel': field.label
                    }
                  ));
                }

                // Additionally, check against allowedExtensions...
                if (allowedExtensions.length && !allowedExtensions.indexOf(type) === -1) {
                  errors.push(Drupal.t(
                    '"!type" is not in the list of allowed file extensions (!allowed) on field "!fieldlabel"',
                    {
                      '!type': type,
                      '!allowed': allowedExtensions.join(', '),
                      '!fieldlabel': field.label
                    }
                  ));
                }
              }
            }

            if (!field.settings.path.length) {
              errors.push(Drupal.t(
                'Please check the filesystem path on field "!fieldlabel"',
                {'!fieldlabel': field.label}
              ));
            }

            if (errors.length) {
              return errors;
            }
            else {
              return true;
            }
          },

          /**
           * Implements vfc callback getFormElement().
           */
          getFormElement: function(varname, config, data, attributes) {
            // Since we will be using jQuery for our uploads, we need to depend on a version >= 1.5.
            var jQv = $.fn.jquery.split('.'),
                jQMv = parseInt(jQv[0]),
                jQmv = parseInt(jQv[1]),
                jQs = jQMv > 1 || (jQMv == 1 && jQmv >= 5);

            if (jQs && FileReader !== undefined) {
              var id = Drupal.vfc_helper.generateUniqueID(),
                  file;

              if (data !== null) {
                if (typeof data == 'string') {
                  file = Drupal.settings.virtual_field_collection_file[varname][data];
                } else {
                  file = data;
                }
              } else {
                file = null;
              }

              return Drupal.theme(
                data === null ? 'vfcFileFormElement': 'vfcFileFieldDisplay',
                id,
                varname,
                config,
                file,
                attributes
              );
            }
            else {
              alert(
                Drupal.t('Critical error: ' +
                  (jQs
                    ? 'Your browser does not support uploading files through ajax. This is required for field "!label".'
                    : 'The installed version of jQuery (installed: !jQv) is not sufficient. Please contact the site administrator!'),
                  {
                    '!jQv': $.fn.jquery,
                    '!label': config.label
                  }
                )
              );
              return '<div><p>' + Drupal.t('FATAL ERROR: input element missing due to software incompatibility!') + '</p></div>';
            }
          },

          /**
           * Event handler function.
           *
           * @param e
           */
          changeEventFileselect: function(e) {
            var $fileinput = $(this),
                $filefield = $fileinput.parents().filter('div.vfcFileupload'),
                id = $fileinput.attr('id').split('-').pop(),
                configID = $filefield.attr('data-fieldid'),
                $upload = $('button#upload-' + id, $filefield),
                $reset = $('button#reset-' + id, $filefield),
                $output = $('#output-' + id, $filefield),
                file = $fileinput.get(0).files[0];
            if (file !== undefined) {
              $upload.show();
              $reset.show();
              Drupal.vfc.fieldapi.file.readFile(
                id,
                configID,
                file,
                Drupal.vfc.fieldapi.file.theme.vfcFileupload
              );
            } else {
              $upload.hide();
              $reset.hide();
              $output.empty();
            }
          },

          /**
           * Event handler function.
           *
           * @param e
           */
          buttonEventUpload: function(e) {
            var $button = $(this),
                id = $button.attr('id').split('-').pop(),
                $filefield = $button.parents().filter('div#vfcFileupload-' + id),
                $fileinput = $('input#file-' + id, $filefield),
                configID = $filefield.attr('data-fieldid'),
                vfc_name = $button.parents('div.vfcAdminArea').attr('data-instancename'),
                $upload = $('button#upload-' + id, $filefield),
                $reset = $('button#reset-' + id, $filefield),
                $progress = $('#progress-' + id, $filefield);
            e.preventDefault();
            e.stopPropagation();
            $upload.hide();
            $reset.hide();
            $progress.show();
            Drupal.vfc.fieldapi.file.uploadFile($fileinput, vfc_name, configID);
          },

          /**
           * Event handler function.
           *
           * @param e
           */
          buttonEventReset: function(e) {
            var $button = $(this),
                id = $button.attr('id').split('-').pop(),
                $filefield = $button.parents().filter('div.vfcFileupload'),
                $fileinput = $('input#file-' + id, $filefield),
                $upload = $('button#upload-' + id, $filefield),
                $reset = $('button#reset-' + id, $filefield),
                $output = $('#output-' + id, $filefield);
            e.preventDefault();
            e.stopPropagation();
            $output.empty();
            $fileinput.replaceWith($fileinput.clone(true));
            $upload.hide();
            $reset.hide();
          },

          /**
           * Event handler function.
           *
           * @param e
           *
           * TODO
           * - do not delete files directly, only on saving...
           */
          buttonEventRemove: function(e) {
            var $button = $(this),
                id = $button.attr('id').split('-').pop(),
                vfc_name = $button.parents('div.vfcAdminArea').attr('data-instancename'),
                varname = $button.attr('data-name'),
                configID = $button.parents().filter('.vfcFileupload').attr('data-fieldid'),
                config = Drupal.vfc.retrieveElementConfiguration(vfc_name, configID),
                fileID = $button.attr('data-fileid');
            e.preventDefault();
            e.stopPropagation();

            var filestodelete = [];
            if ($('div#vfcFileupload-' + id).attr('data-filestodelete') != undefined) {
              filestodelete.push($('div#vfcFileupload-' + id).attr('data-filestodelete').split(','));
            }
            filestodelete.push(fileID);

            $('div#vfcFileupload-' + id).replaceWith(
              Drupal.theme('vfcFileFormElement', id, varname, config, {})
            );

            $('div#vfcFileupload-' + id).attr({
              'data-filestodelete': filestodelete.join(',')
            });

            var $filefield = $('div#vfcFileupload-' + id),
                $fileinput = $('input.form-file', $filefield),
                $upload = $('#upload-' + id, $filefield),
                $reset = $('#reset-' + id, $filefield);

            $fileinput.bind('change', Drupal.vfc.fieldapi.file.changeEventFileselect);
            $upload.bind('click', Drupal.vfc.fieldapi.file.buttonEventUpload);
            $reset.bind('click', Drupal.vfc.fieldapi.file.buttonEventReset);
          },

          /**
           * Implements vfc callback afterBuild().
           */
          afterBuild: function($input, config) {
            var $fileinput = $('input.form-file', $input);
            if ($fileinput.length) {
              var id = $fileinput.attr('id').split('-').pop(),
                  $upload = $('#upload-' + id, $input),
                  $reset = $('#reset-' + id, $input);

              // onChange-behaviour for the file select:
              // Renders some bief information on the file before being uploaded.
              $fileinput.bind('change', Drupal.vfc.fieldapi.file.changeEventFileselect);

              // onClick-behaviour for the "upload"-button.
              $upload.bind('click', Drupal.vfc.fieldapi.file.buttonEventUpload);

              // onClick-behaviour for the "reset"-button.
              $reset.bind('click', Drupal.vfc.fieldapi.file.buttonEventReset);
            } else {
              var $button = $('button.removeFile', $input);

              // onClick-behaviour for the "removeFile" button.
              $button.bind('click', Drupal.vfc.fieldapi.file.buttonEventRemove);
            }
          },

          /**
           * Wrapper function for the html5 FileReader.
           *
           * @param id
           * @param configID
           * @param file
           * @param callable
           */
          readFile: function(id, configID, file, callable) {
            var reader = new FileReader();
            reader.onload = (function(id, configID, file, callable) {
              return function(e) {
                callable(id, configID, e, file);
              };
            })(id, configID, file, callable);
            reader.readAsDataURL(file);
          },

          /**
           * Upload handler function.
           */
          uploadFile: function($fileinput, vfc_name, configID, async, afterUploadCallable) {
            if (async === undefined || typeof async !== 'boolean') {
              async = true;
            }
            if (afterUploadCallable === undefined) {
              var afterUpload = Drupal.vfc.fieldapi.file.afterUpload;
            } else {
              var afterUpload = afterUploadCallable;
            }
            var file = $fileinput.get(0).files[0],
                formData = new FormData(),
                url = Drupal.settings.virtual_field_collection_file.ajax_upload_url + vfc_name + '/' + configID,
                id = $fileinput.attr('id').split('-').pop();

            // @see file.inc, file_save_upload(), line 1448
            formData.append('files[file]', file);

            $.ajax({
              url: url,
              type: 'POST',
              xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                  myXhr.upload.addEventListener(
                    'progress',
                    function(e) {
                      Drupal.vfc.fieldapi.file.uploadProgress(e, id, configID);
                    },
                    false
                  );
                }
                return myXhr;
              },
              cache: false,
              async: async,
              success: function(response, status, xhrObj) {
                Drupal.vfc.fieldapi.file.afterUpload(response, id, configID, status, xhrObj);
              },
              error: function(xhrObj, status, errorDetails) {
                Drupal.vfc.fieldapi.file.uploadError(xhrObj, id, configID, status, errorDetails);
              },
              data: formData,
              dataType: 'json',
              contentType: false,
              processData: false
            });
          },

          /**
           * Displays the progress of a fileupload.
           *
           * @param e
           * @param id
           * @param configID
           */
          uploadProgress: function(e, id, configID) {
            if (e.lengthComputable) {
              $('#progress-' + id).attr({
                value: e.loaded,
                max: e.total
              });
            }
          },

          /**
           * Upload error handler.
           *
           * TODO
           *
           * @param xhrObj
           * @param id
           * @param configID
           * @param status
           * @param errorDetails
           */
          uploadError: function(xhrObj, id, configID, status, errorDetails) {
            console.log(xhrObj);
            console.log(id);
            console.log(configID);
            console.log(status);
            console.log(errorDetails);
          },

          /**
           * Callback function after a successful upload.
           */
          afterUpload: function(response, id, configID, status, xhrObj) {
            var $fileinput = $('input#file-' + id),
                $filefield = $fileinput.parents().filter('div.vfcFileupload'),
                varname = $fileinput.attr('data-name'),
                vfc_name = $fileinput.parents().filter('div.vfcAdminArea').attr('data-instancename'),
                config = Drupal.vfc.retrieveElementConfiguration(vfc_name, configID),
                filestodelete;

            if ($filefield.attr('data-filestodelete') !== undefined) {
              filestodelete = $filefield.attr('data-filestodelete').split(',');
            } else {
              filestodelete = [];
            }

            $filefield.replaceWith(
              Drupal.theme('vfcFileFieldDisplay', id, varname, config, response)
            );

            var $filefield = $('div#vfcFileupload-' + id),
                $removeFile = $('button.removeFile', $filefield);

            $removeFile.bind('click', Drupal.vfc.fieldapi.file.buttonEventRemove);

            if (filestodelete.length) {
              $filefield.attr({
                'data-filestodelete': filestodelete.join(',')
              });
            }
          },

          /**
           * Implements vfc callback checkFieldEmpty().
           */
          checkFieldEmpty: function($input, config) {
            var $fileinput = $('input', $input);
            if ($fileinput.length) {
              return $fileinput.get(0).files.length === 0;
            } else {
              return false;
            }
          },

          /**
           * Implements vfc callback validateField().
           */
          validateField: function($input, config) {
            var $fileinput = $('input', $input);
            if ($fileinput.length) {
              var errors = [],
                  allowedExtensions = config.settings.types.split(' '),
                  file = $fileinput.get(0).files[0],
                  filename = file.name,
                  extension = '.' + filename.split('.').pop().toLowerCase();

              if (allowedExtensions.indexOf('.*') === -1 && allowedExtensions.indexOf(extension) === -1) {
                errors.push(Drupal.t(
                  'The file chosen on field "!fieldlabel" is in the wrong format.',
                  {'!fieldlabel': config.label}
                ));
              }

              if (errors.length) {
                return errors;
              }
              else {
                return true;
              }
            } else {
              // Already uploaded files...
              return true;
            }
          },

          /**
           * Implements vfc callback prepareSerialization().
           */
          prepareSerialization: function($input, config) {
            var $fileinput = $('input.form-file', $input),
                configID = $input.attr('data-fieldid'),
                vfc_name = $fileinput.parents().filter('.vfcAdminArea').attr('data-instancename');

            if ($input.attr('data-filestodelete') !== undefined) {
              var filestodelete = $input.attr('data-filestodelete').split(',');
              for (var index in filestodelete) {
                Drupal.vfc.processAfterSubmit({
                  module: 'virtual_field_collection_file',
                  hook: 'remove_file',
                  args: [vfc_name, configID, filestodelete[index]]
                });
              }
            }

            if ($fileinput.length) {
              // There is a file input... needs a file to be uploaded?
              var file = $fileinput.get(0).files[0];
              if (file !== undefined) {
                var id = $fileinput.attr('id').split('-').pop();

                // Make a blocking, synchronous file upload...
                Drupal.vfc.fieldapi.file.uploadFile(
                  $fileinput,
                  vfc_name,
                  configID,
                  false
                );
              }
            }
          },

          /**
           * Implements vfc callback getFieldValue().
           *
           * TODO
           */
          getFieldValue: function($input, config) {
            var id = $input.attr('id').split('-').pop(),
                $removeButton = $('button#removeFile-' + id);
            if ($removeButton) {
              return $removeButton.attr('data-fileid');
            } else {
              return NULL;
            }
          },

          /**
           * a collection of theme hooks used by this interface
           */
          theme: {
            /**
             * Themes the HTML form elements.
             *
             * @param id
             * @param varname
             * @param config
             * @param data
             * @param attributes
             * @returns {string}
             */
            vfcFileFormElement: function(id, varname, config, data, attributes) {
              if (attributes == undefined) {
                attributes = {};
              }
              var attributeString = [];
              for (var attribute in attributes) {
                attributeString[attribute] = attribute + '="' + attributes[attribute] + '"';
              }
              if (config.settings.types != '.*' && attributeString['accept'] === undefined) {
                attributeString['accept'] = 'accept="' + config.settings.types.split(' ').join(', ') + '"';
              }

              if (Drupal.vfc_helper.getObjectLength(attributeString)) {
                var tmp = '';
                for (var index in attributeString) {
                  tmp += ' ' + attributeString[index];
                }
                attributeString = tmp;
              } else {
                attributeString = '';
              }

              return '<div id="vfcFileupload-' + id + '" class="vfcFileupload" data-fieldid="' + config.configID + '">' +
                       '<input id="file-' + id + '" type="file" class="form-file" data-name="' + varname + '"' + attributeString + ' multiple="false"/>' +
                       '<div class=""actions" style="float: right;">' +
                       '<button id="upload-' + id + '" style="display: none;">' + Drupal.t('Upload') + '</button> ' +
                       '<button id="reset-' + id + '" style="display: none;">' + Drupal.t('Reset') + '</button>' +
                       '<progress id="progress-' + id + '" hidden="hidden"></progress>' +
                       '</div><div style="clear: both;"></div>' +
                       '<output id="output-' + id + '" style="display: block; margin-top: 10px;"></output>' +
                     '</div>';
            },

            /**
             * This markup is shown when a file is actually uploaded for the current field.
             */
            vfcFileFieldDisplay: function(id, varname, config, data) {
              var prefix = '';
              // Call possible extensions
              if (config.type !== 'file') {
                var themeFunctionName = 'vfc' + (config.type.charAt(0).toUpperCase() + config.type.slice(1)) + 'FieldDisplay';
                if (typeof Drupal.vfc.fieldapi[config.type].theme[themeFunctionName] === 'function') {
                    prefix = '<div style="float: left; margin-right: 10px;">' +
                              Drupal.vfc.fieldapi[config.type].theme[themeFunctionName](id, varname, config, data) +
                              '</div>';
                }
              }
              return '<div id="vfcFileupload-' + id + '" class="vfcFileupload" data-fieldid="' + config.configID + '">' +
                      prefix +
                      '<output id="output-' + id + '">' +
                       Drupal.theme(
                         'vfcFileinfo',
                         id,
                         {
                           name: data.filename,
                           type: data.filemime,
                           lastModifiedDate: new Date(data.timestamp * 1000),
                           size: data.filesize,
                           url: data.url,
                           icon: (data.icon !== undefined ? data.icon : false)
                         }
                       ) +
                     '</output><div style="clear: both;"></div>' +
                     '<button id="removeFile-' + id + '" class="removeFile" data-name="' + varname + '" data-fileid="' + data.fid + '">' + Drupal.t('Remove') + '</button>' +
                     '</div>';
            },

            /**
             * Generates a box with useful information on a selected file.
             *
             * @param {string} id
             *          The unique part of the HTML ID of the file field container.
             * @param {object} file
             *          The javascript file object.
             * @returns {string}
             */
            vfcFileinfo: function(id, file) {
              var name = file.name;
              var type = file.type || Drupal.t('n/a');
              var date = file.lastModifiedDate.toLocaleDateString();
              var link = file.url !== undefined ? file.url : false;
              if (link !== false) {
                name = '<a href="' + link + '" target="_blank">' + name + '</a>';
              }
              switch (true) {
                case file.size > 1024 * 1024:
                  var size = Math.round((file.size / (1024 * 1024)) * 100) / 100;
                  var unit = 'MBytes';
                  break;
                case file.size > 1024:
                  var size = Math.round(file.size / 1024);
                  var unit = 'kBytes';
                  break;
                default:
                  var size = file.size;
                  var unit = 'bytes';
              }

              var info = '<div class="vfcFileinfo">' +
                           Drupal.t('File name') + ': ' + (file.icon !== false ? file.icon + ' ' : '') +
                           '<strong>' + name + '</strong><br/>' +
                           Drupal.t('size') + ': <strong>' + size + ' ' + unit + '</strong>, ' +
                           Drupal.t('type') + ': <strong>' + type + '</strong>' +
                           '<span id="additionalinfo-' + id + '"></span><br/>' +
                           Drupal.t('last modified') + ': <strong>' + date + '</strong>' +
                         '</div>' +
                         '<div style="clear: both;"></div>';

              return info;
            },

            /**
             * Themes the "file info" section when a file is selected.
             *
             * As this function is used as a callable to a closure, it
             * will auto-inject the generated html code into the DOM.
             *
             * @param {string} id
             *          The unique part of the HTML ID of the file field container.
             * @param {object} e
             *          The javascript event object.
             * @param {object} file
             *          The javascript file object.
             * @returns NULL
             */
            vfcFileupload: function(id, configID, e, file) {
              file.icon = $.ajax({
                url: Drupal.settings.virtual_field_collection_file.file_icon_url,
                data: { mime: file.type },
                dataType: 'html',
                type: 'POST',
                async: false,
                cache: false
              }).responseText;
              var info = Drupal.theme('vfcFileinfo', id, file),
                  vfc_name = $('#vfcFileupload-' + id).parents().filter('.vfcAdminArea').attr('data-instancename'),
                  config = Drupal.vfc.retrieveElementConfiguration(vfc_name, configID);
              // Call possible extensions
              if (config.type !== 'file') {
                var themeFunctionName = 'vfc' + (config.type.charAt(0).toUpperCase() + config.type.slice(1)) + 'upload';
                if (typeof Drupal.vfc.fieldapi[config.type].theme[themeFunctionName] === 'function') {
                  info = Drupal.vfc.fieldapi[config.type].theme[themeFunctionName](id, configID, e, file, info);
                }
              }
              $('#output-' + id).html(info);
            }
          }
        }
      });

      jQuery.extend(Drupal.theme.prototype, Drupal.vfc.fieldapi.file.theme);
    }
  }
}(jQuery));