/**
 * @file
 * Contains the javascript api implementation for the vfc text field types.
 */

(function ($) {
  Drupal.behaviors.vfc_text = {
    attach: function (context, settings) {
      jQuery.extend(Drupal.vfc.fieldapi, {

        // Simple text fields.
        textfield: {

          /**
           * Implements vfc callback checkConfiguration().
           */
          checkConfiguration: function(field, definition, $DOMelem) {
            var errors = [];
            var minlength = Number(field.settings.minlength);
            var maxlength = Number(field.settings.maxlength);

            if (minlength !== 0 || maxlength !== 0) {
              if (minlength < 0) {
                errors.push(Drupal.t('The minimum length may not be lower than 0.'));
              }
              if (maxlength < 0) {
                errors.push(Drupal.t('The maximum length may not be lower than 0.'));
              }
              if (maxlength !== 0 && minlength !== 0 && maxlength < minlength) {
                errors.push(Drupal.t('The maximum length must be greater than the minimum length.'));
              }
            }

            if (errors.length) {
              return errors;
            }
            else {
              return true;
            }
          },

          /**
           * Implements vfc callback getFormElement().
           */
          getFormElement: function(varname, config, data) {
            // Escape the data to fit into a "value" attribute.
            data = Drupal.vfc.htmlEscapeString(data);

            // If a maximum length was configured for this textfield,
            // set an appropriate html attribute to prevent longer values.
            if (Number(config.settings.maxlength) !== 0) {
              var maxlength = 'maxlength="' + Number(config.settings.maxlength) + '" ';
            }
            else {
              var maxlength = '';
            }

            // Return the complete form input.
            return '<input type="text" class="form-text" data-name="' + varname + '" ' +
                    maxlength + 'value="' + (data ? data : '') + '"/>';
          },

          /**
           * Implements vfc callback afterBuild().
           */
          afterBuild: function($input, config) {
            // If the element was configured to trim leading and trailing
            // whitespace characters, bind the input element to the trim
            // function.
            if (config.settings.trim) {
              $input.bind('blur', function() {
                $(this).val($(this).val().trim());
              });
            }
          },

          /**
           * Implements vfc callback checkFieldEmpty().
           */
          checkFieldEmpty: function($input, config) {
            return $input.val().length === 0;
          },

          /**
           * Implements vfc callback validateField().
           */
          validateField: function($input, config) {
            var errors = [];
            var minlength = Number(config.settings.minlength);
            var maxlength = Number(config.settings.maxlength);
            var regexp = config.settings.regexp;
            var value = $input.val();

            if (minlength !== 0 && value.length < minlength) {
              errors.push(
                Drupal.t(
                  'Field "!fieldlabel" has to be at least !minchars characters long.',
                  {
                    '!fieldlabel': config.label,
                    '!minchars': minlength
                  }
                )
              );
            } else if (maxlength !== 0 && value.length > maxlength) {
              errors.push(
                Drupal.t(
                  'Field "!fieldlabel" has an allowed maximum length of !maxchars characters.',
                  {
                    '!fieldlabel': config.label,
                    '!minchars': maxlength
                  }
                )
              );
            }

            if (regexp !== undefined && regexp !== '') {

              var pattern = new RegExp(regexp);
              if (!pattern.test(value)) {
                errors.push(
                  Drupal.t(
                    'Field "!fieldlabel" is not in the expected format.',
                    {
                      '!fieldlabel': config.label
                    }
                  )
                );
              }
            }

            if (errors.length) {
              return errors;
            }
            else {
              return true;
            }
          },

          /**
           * Implements vfc callback prepareSerialization().
           */
          prepareSerialization: function($input, config) {

          },

          /**
           * Implements vfc callback getFieldValue().
           */
          getFieldValue: function($input, config) {
            return $input.val();
          }

        },

        // Text areas.
        textarea: {

          /**
           * Implements vfc callback checkConfiguration().
           */
          checkConfiguration: function(field, definition, $DOMelem) {
            var errors = [];
            var minlength = Number(config.settings.minlength);
            var maxlength = Number(config.settings.maxlength);

            if (minlength !== 0 || maxlength !== 0) {
              if (minlength < 0) {
                errors.push(Drupal.t('The minimum length may not be lower than 0.'));
              }
              if (maxlength < 0) {
                errors.push(Drupal.t('The maximum length may not be lower than 0.'));
              }
              if (maxlength !== 0 && minlength !== 0 && maxlength < minlength) {
                errors.push(Drupal.t('The maximum length must be greater than the minimum length.'));
              }
            }

            if (errors.length) {
              return errors;
            }
            else {
              return true;
            }
          },

          /**
           * Implements vfc callback getFormElement().
           */
          getFormElement: function(varname, config, data) {
            // Escape the data to fit into a "value" attribute.
            data = Drupal.vfc.htmlEscapeString(data);

            // Return the complete form input.
            return '<textarea class="form-textarea" data-name="' + varname + '">' +
                   (data ? data : '') +
                   '</textarea>';
          },

          /**
           * Implements vfc callback afterBuild().
           */
          afterBuild: function($input, config) {
            // If the element was configured to trim leading and trailing
            // whitespace characters, bind the input element to the trim
            // function.
            if (config.settings.trim) {
              $input.bind('blur', function() {
                $(this).val($(this).val().trim());
              });
            }
          },

          /**
           * Implements vfc callback checkFieldEmpty().
           */
          checkFieldEmpty: function($input, config) {
            return $input.val().length === 0;
          },

          /**
           * Implements vfc callback validateField().
           */
          validateField: function($input, config) {
            var errors = [];
            var minlength = Number(config.settings.minlength);
            var maxlength = Number(config.settings.maxlength);
            var value = $input.val();

            if (minlength !== 0 && value.length < minlength) {
              errors.push(
                Drupal.t(
                  'Field "!fieldlabel" has to be at least !minchars characters long.',
                  {
                    '!fieldlabel': config.label,
                    '!minchars': minlength
                  }
                )
              );
            } else if (maxlength !== 0 && value.length > maxlength) {
              errors.push(
                Drupal.t(
                  'Field "!fieldlabel" has an allowed maximum length of !maxchars characters.',
                  {
                    '!fieldlabel': config.label,
                    '!minchars': maxlength
                  }
                )
              );
            }

            if (errors.length) {
              return errors;
            }
            else {
              return true;
            }
          },

          /**
           * Implements vfc callback prepareSerialization().
           */
          prepareSerialization: function($input, config) {

          },

          /**
           * Implements vfc callback getFieldValue().
           */
          getFieldValue: function($input, config) {
            return $input.val();
          }

        }
      });
    }
  }
}(jQuery));
