/**
 * @file
 * Contains the javascript api implementation for the vfc reference field types.
 */

(function ($) {
  Drupal.behaviors.vfc_reference = {
    attach: function (context, settings) {
      jQuery.extend(Drupal.vfc.fieldapi, {

        // Node reference field type.
        nodereference: {

          /**
           * Implements vfc callback checkConfiguration().
           */
          checkConfiguration: function(field, definition, $DOMelem) {
            var errors = [];

            if (!field.settings.types.length) {
              errors.push(Drupal.t(
                'You have to select at least one node type on field "!fieldlabel"',
                {'!fieldlabel': field.label}
              ));
            }

            if (!field.settings.widget.length) {
              errors.push(Drupal.t(
                'Please select the widget type to use for field "!fieldlabel".',
                {'!fieldlabel': field.label}
              ));
            }

            if (errors.length) {
              return errors;
            }
            else {
              return true;
            }
          },

          /**
           * Implements vfc callback getFormElement().
           */
          getFormElement: function(varname, config, data) {
            switch (config.settings.widget) {
              case 'autocomplete':
                if (data !== undefined && data !== '' && data !== null) {
                  var datavalue = ' data-value="' + data + '"';
                  var value = Drupal.settings.virtual_field_collection.vfc_reference[varname];
                }
                else {
                  var datavalue = '';
                  var value = '';
                }
                return '<input type="text" class="form-text" data-name="' +
                       varname + '" value="' + value + '"' + datavalue + '/>';

              case 'select':
                var select = '<select data-name="' + varname + '">' +
                             '<option value="">' + Drupal.t('Please select') + '</option>';
                var lasttype = null;
                for (var index in Drupal.settings.virtual_field_collection.vfc_reference[varname]) {
                  var node = Drupal.settings.virtual_field_collection.vfc_reference[varname][index];
                  if (Number(node.value) === Number(data)) {
                    var selected = ' selected="selected"';
                  }
                  else {
                    var selected = '';
                  }

                  if (node.category !== lasttype) {
                    if (lasttype !== null) {
                      select += '</optgroup>';
                    }
                    select += '<optgroup label="' + Drupal.t('Node type') + ' &quot;' + node.category + '&quot;">';
                    lasttype = node.category;
                  }

                  select += '<option value="' + node.value + '"' + selected + '>' +
                    node.label +
                    '</option>';
                }
                if (lasttype !== null) {
                  select += '</optgroup>';
                }
                select += '</select>';
                return select;

            }
          },

          /**
           * Implements vfc callback afterBuild().
           */
          afterBuild: function($input, config) {
            if (config.settings.widget === 'autocomplete') {
              Drupal.vfc.attachAutoComplete(
                $input,
                'virtual_field_collection_reference_virtualfield_autocomplete',
                config.settings.types.join(','),
                'node'
              );
            }
          },

          /**
           * Implements vfc callback checkFieldEmpty().
           */
          checkFieldEmpty: function($input, config) {
            if (config.settings.widget === 'autocomplete') {
              return $input.attr('data-value') === undefined;
            }
            else if (config.settings.widget === 'select') {
              return $('option:selected', $input).val() === '';
            }
          },

          /**
           * Implements vfc callback validateField().
           */
          validateField: function($input, config) {
            // Due to the fact that we don't need to expect empty
            // field to be validated (vfc handles this on it's own),
            // all values will be considered valid, so we'll simply
            // return true.
            return true;
          },

          /**
           * Implements vfc callback prepareSerialization().
           */
          prepareSerialization: function($input, config) {

          },

          /**
           * Implements vfc callback getFieldValue().
           */
          getFieldValue: function($input, config) {
            if (config.settings.widget === 'autocomplete') {
              return $input.attr('data-value');
            }
            else if (config.settings.widget === 'select') {
              return $('option:selected', $input).val();
            }
          }

        },

        // User reference field type.
        userreference: {

          /**
           * Implements vfc callback checkConfiguration().
           */
          checkConfiguration: function(field, definition, $DOMelem) {
            // This field has no mandatory settings.
            return true;
          },

          /**
           * Implements vfc callback getFormElement().
           */
          getFormElement: function(varname, config, data) {
            switch (config.settings.widget) {
              case 'autocomplete':
                if (data !== undefined && data !== '' && data !== null) {
                  var datavalue = ' data-value="' + data + '"';
                  var value = Drupal.settings.virtual_field_collection.vfc_reference[varname];
                }
                else {
                  var datavalue = '';
                  var value = '';
                }
                return '<input type="text" class="form-text" data-name="' +
                        varname + '" value="' + value + '"' + datavalue + '/>';

              case 'select':
                var select = '<select data-name="' + varname + '">' +
                             '<option value="">' + Drupal.t('Please select') + '</option>';
                for (var index in Drupal.settings.virtual_field_collection.vfc_reference[varname]) {
                  var user = Drupal.settings.virtual_field_collection.vfc_reference[varname][index];
                  if (Number(user.value) === Number(data)) {
                    var selected = ' selected="selected"';
                  }
                  else {
                    var selected = '';
                  }
                  select += '<option value="' + user.value + '"' + selected + '>' +
                    user.label +
                    '</option>';
                }
                select += '</select>';
                return select;

            }
          },

          /**
           * Implements vfc callback afterBuild().
           */
          afterBuild: function($input, config) {
            if (config.settings.widget === 'autocomplete') {
              var filter = [
                config.settings.roles.join(','),
                config.settings.accountstate
              ];
              Drupal.vfc.attachAutoComplete(
                $input,
                'virtual_field_collection_reference_virtualfield_autocomplete',
                filter.join('-'),
                'user'
              );
            }
          },

          /**
           * Implements vfc callback checkFieldEmpty().
           */
          checkFieldEmpty: function($input, config) {
            if (config.settings.widget === 'autocomplete') {
              return $input.attr('data-value') === undefined;
            }
            else if (config.settings.widget === 'select') {
              return $('option:selected', $input).val() === '';
            }
          },

          /**
           * Implements vfc callback validateField().
           */
          validateField: function($input, config) {
            // Due to the fact that we don't need to expect empty
            // field to be validated (vfc handles this on it's own),
            // all values will be considered valid, so we'll simply
            // return true.
            return true;
          },

          /**
           * Implements vfc callback prepareSerialization().
           */
          prepareSerialization: function($input, config) {

          },

          /**
           * Implements vfc callback getFieldValue().
           */
          getFieldValue: function($input, config) {
            if (config.settings.widget === 'autocomplete') {
              return $input.attr('data-value');
            }
            else if (config.settings.widget === 'select') {
              return $('option:selected', $input).val();
            }
          }

        },

        // Term reference field type.
        termreference: {

          /**
           * Implements vfc callback checkConfiguration().
           */
          checkConfiguration: function(field, definition, $DOMelem) {
            var errors = [];

            if (!field.settings.vocabs.length) {
              errors.push(Drupal.t(
                'You have to select at least one vocabulary on field "!fieldlabel"',
                {'!fieldlabel': field.label}
              ));
            }

            if (!field.settings.widget.length) {
              errors.push(Drupal.t(
                'Please select the widget type to use for field "!fieldlabel".',
                {'!fieldlabel': field.label}
              ));
            }

            if (errors.length) {
              return errors;
            }
            else {
              return true;
            }
          },

          /**
           * Implements vfc callback getFormElement().
           */
          getFormElement: function(varname, config, data) {
            switch (config.settings.widget) {
              case 'autocomplete':
                if (data !== undefined && data !== '' && data !== null) {
                  var datavalue = ' data-value="' + data + '"';
                  var value = Drupal.settings.virtual_field_collection.vfc_reference[varname]['tid-' + data];
                }
                else {
                  var datavalue = '';
                  var value = '';
                }
                return '<input type="text" class="form-text" data-name="' +
                        varname + '" value="' + value + '"' + datavalue + '/>';

              case 'select':
                var select = '<select data-name="' + varname + '">' +
                             '<option value="">' + Drupal.t('Please select') + '</option>';
                var lastvocab = null;
                for (var index in Drupal.settings.virtual_field_collection.vfc_reference[varname]) {
                  var term = Drupal.settings.virtual_field_collection.vfc_reference[varname][index];
                  if (Number(term.value) === Number(data)) {
                    var selected = ' selected="selected"';
                  }
                  else {
                    var selected = '';
                  }

                  if (term.category !== lastvocab) {
                    if (lastvocab !== null) {
                      select += '</optgroup>';
                    }
                    select += '<optgroup label="' + Drupal.t('Vocabulary') + ' &quot;' + term.category + '&quot;">';
                    lastvocab = term.category;
                  }

                  select += '<option value="' + term.value + '"' + selected + '>' +
                              term.label +
                            '</option>';
                }
                if (lastvocab !== null) {
                  select += '</optgroup>';
                }
                select += '</select>';
                return select;

            }
          },

          /**
           * Implements vfc callback afterBuild().
           */
          afterBuild: function($input, config) {
            if (config.settings.widget === 'autocomplete') {
              var vocabs = [];
              for (var index in config.settings.vocabs) {
                vocabs.push(config.settings.vocabs[index]);
              }
              Drupal.vfc.attachAutoComplete(
                $input,
                'virtual_field_collection_reference_virtualfield_autocomplete',
                vocabs.join(','),
                'taxonomy'
              );
            }
          },

          /**
           * Implements vfc callback checkFieldEmpty().
           */
          checkFieldEmpty: function($input, config) {
            if (config.settings.widget === 'autocomplete') {
              return $input.attr('data-value') === undefined;
            }
            else if (config.settings.widget === 'select') {
              return $('option:selected', $input).val() === '';
            }
          },

          /**
           * Implements vfc callback validateField().
           */
          validateField: function($input, config) {
            // Due to the fact that we don't need to expect empty
            // field to be validated (vfc handles this on it's own),
            // all values will be considered valid, so we'll simply
            // return true.
            return true;
          },

          /**
           * Implements vfc callback prepareSerialization().
           */
          prepareSerialization: function($input, config) {

          },

          /**
           * Implements vfc callback getFieldValue().
           */
          getFieldValue: function($input, config) {
            if (config.settings.widget === 'autocomplete') {
              return $input.attr('data-value');
            }
            else if (config.settings.widget === 'select') {
              return $('option:selected', $input).val();
            }
          }

        }
      });
    }
  }
}(jQuery));
