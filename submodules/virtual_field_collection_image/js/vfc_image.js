/**
 * @file
 * Contains the javascript api implementation for the vfc image field types.
 *
 * Mostly this will only be a proxy to the vfc_file js interface.
 */

(function ($) {
  Drupal.behaviors.vfc_image = {
    attach: function (context, settings) {

      jQuery.extend(Drupal.vfc.fieldapi, {

        // Normal file uploads.
        image: {

          /**
           * Implements vfc callback checkConfiguration().
           */
          checkConfiguration: function(field, definition, $DOMelem) {
            return Drupal.vfc.fieldapi.file.checkConfiguration(
              field,
              definition,
              $DOMelem,
              ['.jpg', '.jpeg', '.png', '.gif']
            );
          },

          /**
           * Implements vfc callback getFormElement().
           */
          getFormElement: function(varname, config, data) {
            data = Drupal.settings.virtual_field_collection_image[varname][data];
            return Drupal.vfc.fieldapi.file.getFormElement(varname, config, data, {'accept': 'image/*'});
          },

          /**
           * Implements vfc callback afterBuild().
           */
          afterBuild: function($input, config) {
            // Common things...
            Drupal.vfc.fieldapi.file.afterBuild($input, config);
          },

          /**
           * Implements vfc callback checkFieldEmpty().
           */
          checkFieldEmpty: function($input, config) {
            return Drupal.vfc.fieldapi.file.checkFieldEmpty($input, config);
          },

          /**
           * Implements vfc callback validateField().
           */
          validateField: function($input, config) {
            return Drupal.vfc.fieldapi.file.validateField($input, config);
          },

          /**
           * Implements vfc callback prepareSerialization().
           *
           * TODO
           */
          prepareSerialization: function($input, config) {
            Drupal.vfc.fieldapi.file.prepareSerialization($input, config);
          },

          /**
           * Implements vfc callback getFieldValue().
           *
           * TODO
           */
          getFieldValue: function($input, config) {
            return Drupal.vfc.fieldapi.file.getFieldValue($input, config);
          },

          /**
           * a collection of theme hooks used by this interface
           */
          theme: {
            /**
             * Themes the "file info" section when a file is selected.
             *
             * This function will get called by the field type "file" automatically.
             *
             * @param {string} id
             *          The unique part of the HTML ID of the file field container.
             * @param {string} configID
             *          The configuration ID of the actual field.
             * @param {object} e
             *          The javascript event object.
             * @param {object} file
             *          The javascript file object.
             * @returns {string} HTML
             */
            vfcImageupload: function(id, configID, e, file) {
              var info = Drupal.theme('vfcFileinfo', id, file), preview = '';
              if (e && file.type.match(/^image/i)) {
                var image = new Image();
                image.src = e.target.result;
                image.onload = (function(id) {
                  return function() {
                    $('#additionalinfo-' + id).html(', <strong>' + this.width + 'x' + this.height + ' px</strong>');
                  }
                })(id);
                preview = [
                  '<img class="thumb" style="max-width: 100px; max-height: 75px; float: left; margin-right: 10px;" src="',
                  e.target.result,
                  '" title="',
                  escape(file.name),
                  '"/>'
                ].join('');
              }
              return preview + info;
            },

            /**
             * Supports/complements the "file info" section when a file is already uploaded.
             *
             * This function will get called by the field type "file" automatically.
             *
             * @param id
             * @param varname
             * @param config
             * @param data
             * @returns {string} HTML
             */
            vfcImageFieldDisplay: function(id, varname, config, data) {
              return data.thumbnail !== undefined ? data.thumbnail : '';
            }
          }
        }
      });

      jQuery.extend(Drupal.theme.prototype, Drupal.vfc.fieldapi.image.theme);
    }
  }
}(jQuery));