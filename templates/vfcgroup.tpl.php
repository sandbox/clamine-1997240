<?php if ($wrapperElement !== 'none'): ?>
<<?php print $wrapperElement; ?><?php if (!empty($wrapperCssClass)): ?> class="<?php print $wrapperCssClass; ?>"<?php endif; ?>>
<?php endif; ?>

  <?php if ($showLabel && !empty($label)): ?>
    <?php print "<$labelWrapper>$label</$labelWrapper>"; ?>
  <?php endif; ?>

  <?php if ($groupWrapper !== 'none'): ?>
  <?php print "<$groupWrapper>"; ?>
  <?php endif; ?>

    <?php foreach ($entries as $entry): ?>

      <?php if (in_array($groupWrapper, array('ul', 'ol'))): ?>
      <li<?php if (!empty($entryCssClass)): ?> class="<?php print $entryCssClass; ?>"<?php endif; ?>>
      <?php endif; ?>

        <?php print render($entry); ?>

      <?php if (in_array($groupWrapper, array('ul', 'ol'))): ?>
      </li>
      <?php endif; ?>

    <?php endforeach; ?>

  <?php if ($groupWrapper !== 'none'): ?>
  <?php print "</$groupWrapper>"; ?>
  <?php endif; ?>

<?php if ($wrapperElement !== 'none'): ?>
<?php print "</$wrapperElement>"; ?>
<?php endif; ?>