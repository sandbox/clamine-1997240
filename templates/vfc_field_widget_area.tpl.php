<?php
/**
 * @file
 * This template contains the markup for the virtual field collection
 * field widget on the entity edit forms. This is a blueprint for the
 * javascript interface implementation on which virtual field collection
 * fields are edited.
 *
 * Note:
 * It is important not to change the html structure of this markup, as
 * the javascript interface implementation will rely on it in order to
 * find the appropriate elements.
 */
?>
<div class="form-item vfcAdminArea">
  <label><?php print $field_label; ?></label>
  <div class="vfcMessages" style="display: none;"></div>
  <div class="vfcElement vfcElementRoot" data-type="vfcRoot">
    <div class="vfcContainer vfcRoot">
      <div class="noJS"><p><?php print t('Virtual Field Collection needs JavaScript to work!'); ?></p></div>
    </div>
  </div>
</div>
