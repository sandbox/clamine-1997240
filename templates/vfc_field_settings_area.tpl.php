<?php
/**
 * @file
 * This file contains the markup appended to the field settings form.
 * This is a blueprint for the javascript interface implementation
 * on which virtual field collection fields are configured.
 *
 * Note:
 * It is important not to change the html structure of this markup, as
 * the javascript interface implementation will rely on it in order to
 * find the appropriate elements.
 */
?>
<div id="vfcAdminArea">
  <div class="description">
    <p><?php print t('Hints'); ?>:</p>
    <ul>
      <li>Use <em>groups</em> to create fields or collection of fields that can have multiple entries.</li>
      <li><em>Groups</em> do already come with an optional label field and provide settings for minimum and maximum entry count.</li>
      <li>Use drag and drop to reorder your elements within your groups.</li>
      <li>The green boundary indicates the active container that is used to initially target the placement for a new element. Click on a group or the root element to select your desired target.<li>
    </ul>
    <?php if ($delete_warning): ?>
      <p>
        <strong><?php print t('IMPORTANT NOTICE'); ?>:</strong><br/>
        <?php print t('If you delete fields from this virtual field collection, the data for the deleted items will NOT be deleted from the database at once, but the fields will be neither made accessible on the frontend nor the backend!'); ?>
      </p>
    <?php endif; ?>
  </div>
  <div class="vfcOptions">
    <button class="vfcButton" data-action="addGroup"><?php print t('Add Group'); ?></button>
    <?php if (!empty($add_field_options)): ?>
      <?php print t('or'); ?>
      <?php print $add_field_options; ?>
      <button class="vfcButton" data-action="addField"><?php print t('Add Field'); ?></button>
    <?php else: ?>
      <span><?php print t('No sub-modules providing fields have been activated for virtual field collection! Visit the !module_administration_pages to activate sub-modules.', array('!module_administration_pages' => l(t('module administration pages'), 'admin/modules'))); ?></span>
    <?php endif; ?>
  </div>
  <div class="vfcMessages" style="display: none;"></div>
  <div class="vfcElement" data-type="vfcGroup">
    <div class="vfcElementHeader vfcElementRootHeader">
      <label><?php print t('Virtual Fields'); ?></label>
    </div>
    <div class="vfcContainer vfcRoot">
      <div class="noJS"><p><?php print t('Virtual Field Collection needs JavaScript to work!'); ?></p></div>
    </div>
  </div>
</div>
