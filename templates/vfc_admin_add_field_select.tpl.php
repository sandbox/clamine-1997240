<?php
/**
 * @file
 * This template generates the field selector dropdown on the backend
 * configuration interface. It uses a simple select input without a name
 * attribute.
 */
?>
<?php if (!empty($available_field_types) && is_array($available_field_types)): ?>
<select class="vfc_add_field">
  <option value=""><?php print t('Select field type to add'); ?></option>
  <?php foreach ($available_field_types as $group => $fields): ?>
    <optgroup label="<?php print $group; ?>">
      <?php foreach ($fields as $fieldname => $definition): ?>
        <option value="<?php print $fieldname; ?>">
          <?php print $definition['label']; ?>
        </option>
      <?php endforeach; ?>
    </optgroup>
  <?php endforeach; ?>
</select>
<?php endif; ?>
