<?php
/**
 * @file
 * This file contains the markup appended to the field formatter
 * settings form. This is a blueprint for the javascript interface
 * implementation on which virtual field collection field display
 * settings are configured.
 *
 * Note:
 * It is important not to change the html structure of this markup, as
 * the javascript interface implementation will rely on it in order to
 * find the appropriate elements.
 */
?>
<div id="vfcAdminArea">
  <div class="vfcElement" data-type="vfcGroup">
    <div class="vfcElementHeader vfcElementRootHeader">
      <label><?php print t('Virtual Fields'); ?></label>
    </div>
    <div class="vfcContainer vfcRoot">
      <div class="noJS"><p><?php print t('Virtual Field Collection needs JavaScript to work!'); ?></p></div>
    </div>
  </div>
</div>
