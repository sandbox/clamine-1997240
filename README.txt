Virtual Field Collection

Description:
  Virtual Field Collection aims at helping to store data not needed
  to be individually addressed in a serialized manner in the site's
  database thus reducing the table count, the database size as well
  as the (sub)queries needed to load an entity. In order to do this,
  virtual field collection provides a custom field type with a field
  settings interface, integrates well with entity edit forms and
  provides a field formatter to configure the rendering of the fields.

History:
  The idea to this module arose from massive performance problems
  while developing a client's project, a huge and massively visited
  recipe site. These problems were partly caused by a huge count of
  Field API fields attached to a specific node type, causing numerous
  complex queries to be sent to the site's database. Regarding the
  fact that the main part of these fields were of no use without the
  containing node entity itself and were not required to be addressed
  individually within views led to the conclusion to reduce the number
  of fields and thus field data tables by serializing them in a single
  field. This custom tailored solution turned out to work very well,
  so this module was developed in order to provide a generic and
  reusable version of this technique.

Module project page:
  https://drupal.org/sandbox/clamine/1997240

To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/1997240

-- MAINTAINERS --

CHiLi.HH - https://drupal.org/user/738402