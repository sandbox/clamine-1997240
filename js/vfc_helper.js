/**
 * @file
 * Contains some javascript helper functions for the virtual field collection javascript
 * interface.
 */

(function ($) {

  Drupal.vfc_helper = {
    /**
     * Helper function: generates a unique ID string.
     *
     * The generated ID will consist of 32 alphanumerical characters.
     *
     * @returns {string}
     */
    generateUniqueID: function() {
      var ID = '';
      for (var i = 0; i < 8; i++) {
        ID += Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
      }
      return ID;
    },

    /**
     * Helper function: calculate the length of a javascript object.
     *
     * Reason for custom implementation:
     * Object.keys() is not supported by all browsers (notably IE < 9 does not
     * support it).
     *
     * @param object
     * @returns {number}
     */
    getObjectLength: function(object) {
      var length = 0;
      for (var property in object) {
        length++;
      }
      return length;
    }
  };

}(jQuery));
