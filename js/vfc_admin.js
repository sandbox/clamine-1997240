/**
 * @file
 * Contains the javascript implementation for the virtual field collection configuration
 * interface.
 */

/**
 * Function library for the admin interface.
 */
(function ($) {

  Drupal.vfc = {

    // Preparation of some local variables.
    form_required_markup: ' <span class="form-required" title="' + Drupal.t('This field is required.') + '">*</span>',
    group_empty_markup: '<div class="noFields"><p>' + Drupal.t('No elements in this group') + '</p></div>',

    /**
     * Fetches the actual configuration array for a specified field type.
     *
     * @param type
     * @returns {*}
     */
    fetchFieldConfig: function(type) {
      for (var module in Drupal.settings.virtual_field_collection.available_field_types) {
        for (var field in Drupal.settings.virtual_field_collection.available_field_types[module]) {
          if (field == type) {
            return Drupal.settings.virtual_field_collection.available_field_types[module][field];
          }
        }
      }
      // no definition matches the searched field.
      return false;
    },

    /**
     * Cache some dom elements to speed things up.
     *
     * This function is called by initInterface, which itself is fired upon $(document).ready
     */
    cacheDOMElements: function($instance) {
      var $form = $($('textarea.vfc_serialized_data').parents().filter('form').get(0));
      var $config_area = $('#vfcAdminArea', $form);
      $form.data('instance', $instance);
      $config_area.data('instance', $instance);
      $instance.data({
        '$form': $form,
        '$config_area': $config_area,
        '$message_area': $('> .vfcMessages', $config_area),
        '$root_container': $('> .vfcElement > .vfcRoot', $config_area),
        '$fieldtype_selector': $('.vfcOptions select', $config_area)
      });
    },

    /**
     * Initially called to build up the interface.
     * Creates field definitions already set and sets root as active container.
     */
    initInterface: function(field) {
      // shortcut to the instance
      var $instance = $(field);

      // One-time shot to cache the VFC DOM elements.
      Drupal.vfc.cacheDOMElements($instance);

      // Bind the vfc submit handler to form submission.
      $instance.data('$form').bind('submit', Drupal.vfc.checkConfiguration);

      // Hide the "Number of values" field - VFC fields are always "one shots".
      $('.form-item-field-cardinality', $instance.data('$form')).hide();

      // Hide the "noJS" warning.
      $('.noJS', $instance.data('$config_area')).remove();

      // Append the "no fields" warning to the root container.
      $instance.data('$root_container').append(Drupal.vfc.group_empty_markup);

      // Initially set vfcRoot as the active container.
      Drupal.vfc.setActiveContainer($instance.data('$root_container'));

      // Initialize already existing groups and fields (if any).
      var config = Drupal.settings.virtual_field_collection.virtual_fields;
      if (Drupal.vfc_helper.getObjectLength(config)) {
        Drupal.vfc.initConfiguration($instance, config, $instance.data('$root_container'));
      }

      // Show the "noFields" warning if no fields are added.
      Drupal.vfc.showHideNoFieldsWarning($instance.data('$root_container'));

      // Re-set vfcRoot as the active container.
      Drupal.vfc.setActiveContainer($instance.data('$root_container'));

      // Initialize click event handlers.
      Drupal.vfc.bindClickEvents($instance);
    },

    /**
     * Initializes an existing field configuration.
     *
     * @param {Object} $instance
     * @param {Array} config
     * @param {HTMLElement} $root
     */
    initConfiguration: function($instance, config, $root) {
      // Process each single entry in the config array.
      for (var varname in config) {
        var element = config[varname];

        // Determine the behaviour based on the current element's type.
        switch (element.type) {
          case 'group':
            // The currently processed element is a group - add the group.
            var $group = Drupal.vfc.addGroup(
              $instance,
              element.groupLabel,
              varname,
              element.buttonLabel,
              element.minEntries,
              element.maxEntries,
              element.groupLabelField,
              element.configID
            );

            // Descend recursively through the group's content.
            Drupal.vfc.initConfiguration($instance, element.fields, $group);

            // Finally, re-set the active container to the current element.
            Drupal.vfc.setActiveContainer($root);
            break;

          default:
            // The actual element is just a plain field.
            Drupal.vfc.addField(
              $instance,
              element.type,
              element.label,
              varname,
              element.required,
              element.description,
              element.settings,
              element.configID
            );
        }
      }
    },

    /**
     * Bind all clickable elements to their respective click handler.
     */
    bindClickEvents: function($instance) {
      // Bind all buttons to the button click event dispatcher.
      $('button.vfcButton', $instance.data('$config_area'))
        .unbind('click')
        .bind('click', function(e) {
          e.preventDefault();
          e.stopPropagation();
          Drupal.vfc.buttonClick($(this), e);
        })
        .data('instance', $instance);

      // Bind all containers to container selection.
      $('.vfcElement[data-type="vfcGroup"]', $instance.data('$config_area'))
        .unbind('click')
        .bind('click', function(e) {
          e.stopPropagation();
          Drupal.vfc.setActiveContainer($(this));
        });

      // Initialize the sorting ability.
      $('.vfcContainer', $instance.data('$config_area')).sortable({
        handle: '.vfcElementHeader',
        containment: 'parent',
        axis: 'y'
      });
    },

    /**
     * Unbind all clickable elements from their respective click handler.
     * This is done to avoid multiple calls upon re-initialization of form elements.
     */
    unbindClickEvents: function($instance) {
      // Remove all buttons from the event dispatcher.
      $('button.vfcButton', $instance.data('$config_area')).unbind('click');

      // Unbind all containers from container selection.
      $('.vfcElement[data-type="vfcGroup"]', $instance.data('$config_area')).unbind('click');

      // Remove the sorting ability.
      $('.vfcContainer', $instance.data('$config_area')).sortable('destroy');
    },

    /**
     * Button click event dispatcher.
     *
     * Catches button clicks and tries to call the appropriate callback
     * function. Will throw a "silent error" (console output) if the
     * callback fails.
     *
     * @param {HTMLElement} $button
     * @param {Object} event
     */
    buttonClick: function($button, event) {
      // Determine the action to take.
      var action = $button.attr('data-action');
      var $instance = $button.data('instance');

      // Remove all existing click event handlers prior to
      // executing the callback to prevent interference and/or
      // possible negative side effects.
      Drupal.vfc.unbindClickEvents($instance);

      // Try to the callback if it exists.
      try {
        Drupal.vfc[action].apply($button, $instance);
      } catch (err) {
        // If the event handler is not defined or fails execution,
        // log a warning message to the console.
        if (Drupal.vfc[action] === undefined) {
          console.log(Drupal.t('Error: No event handler for !action.', {'!action': action}));
        }
        else {
          console.log(Drupal.t('Error') + ': ' + err.message);
        }
      }

      // Re-attach the click event handlers.
      Drupal.vfc.bindClickEvents($instance);
    },

    /**
     * Button Click Callback: adds a group.
     *
     * @param {Object} $instance
     * @param {string} groupLabel (optional)
     * @param {string} varname (optional)
     * @param {string} buttonLabel (optional)
     * @param {int} minEntries (optional)
     * @param {int} maxEntries (optional)
     * @param {boolean} groupLabelField (optional)
     * @param {string} configID (optional)
     * @returns {HTMLElement}
     */
    addGroup: function($instance, groupLabel, varname, buttonLabel, minEntries, maxEntries, groupLabelField, configID) {
      if ($instance instanceof HTMLTextAreaElement) {
        $instance = $($instance);
      }

      // Determine the active target container.
      var $container = Drupal.vfc.getActiveContainer($instance);

      // Get a themed group element with all possible arguments.
      var group = Drupal.theme(
        'vfcGroup',
        groupLabel,
        varname,
        buttonLabel,
        minEntries,
        maxEntries,
        groupLabelField,
        configID
      );

      // Append the new group to the active container.
      var $group = $(group).appendTo($container);

      // Check the "no fields" warning (= make sure it's hidden actually).
      Drupal.vfc.showHideNoFieldsWarning($container);

      // Set this container as the now active container.
      Drupal.vfc.setActiveContainer($group);

      // Return the new group element.
      return $group;
    },

    /**
     * Button Click Callback: deletes a group.
     */
    deleteGroup: function() {
      // Store the clicked button as a more readable shortcut.
      var $button = $(this);

      // Determine the vfcGroup element the button belongs to.
      var $group = $($button.parents().filter('.vfcElement').get(0));

      // Determine the group label (if any was set).
      var glabel = $('input[data-type="groupLabel"]', $group).val().length
                    ? $('input[data-type="groupLabel"]', $group).val()
                    : '<no label>';

      // Determine if the group contains any elements (count them).
      var gElements = $('> .vfcElementContentWrapper > .vfcGroupContentWrapper > .vfcContainer > .vfcElement', $group).length;

      // Prepare the confirmation question.
      if (gElements > 0) {
        var confirmQuestion = Drupal.t(
          'Are you sure to delete the non-empty group "!grouplabel"?',
          {'!grouplabel': glabel }
        );
      }
      else {
        var confirmQuestion = Drupal.t(
          'Are you sure to delete the empty group "!grouplabel"?',
          {'!grouplabel': glabel }
        );
      }

      // Ask for the user's permission to delete the group.
      if (confirm(confirmQuestion)) {
        // Permission was given.

        // Determine the container the group is located in.
        var $container = $group.parent();

        // Remove the group DOM element.
        $group.remove();

        // Check the "no fields" warning inside the container.
        Drupal.vfc.showHideNoFieldsWarning($container);

        // Set the parent container as the new active target container.
        Drupal.vfc.setActiveContainer($($container.parents().filter('.vfcElement').get(0)));
      }
    },

    /**
     * Button Click Callback: adds a field.
     *
     * @param {string} $instance
     * @param {string} type (optional)
     * @param {string} label (optional)
     * @param {string} varname (optional)
     * @param {boolean} required (optional)
     * @param {object} settingValues (optional)
     * @param {string} configID
     */
    addField: function($instance, type, label, varname, required, description, settingValues, configID) {
      if ($instance instanceof HTMLTextAreaElement) {
        $instance = $($instance);
      }

      // Default values for the function arguments -
      // either all or none arguments were given, so set the
      // default values already if the first argument is missing.
      if (type === undefined) {
        type = $instance.data('$fieldtype_selector').val();
        label = '';
        varname = '';
        required = false;
        description = '';
        settingValues = {};
        configID = Drupal.vfc_helper.generateUniqueID();
      }

      // Generate the element only if it is a valid type.
      var fieldconfig = Drupal.vfc.fetchFieldConfig(type);
      if (type.length && settings !== false) {
        // Determine where to place the new field.
        var $container = Drupal.vfc.getActiveContainer($instance);

        // Default set of settings for all kinds of fields.
        var settings = {
          label: {
            label: Drupal.t('Label') + Drupal.vfc.form_required_markup,
            type: 'text',
            default: '',
            value: label
          },
          varname: {
            label: Drupal.t('Variable name') + Drupal.vfc.form_required_markup,
            type: 'text',
            default: '',
            value: varname
          },
          required: {
            label: Drupal.t('Required'),
            type: 'bool',
            default: false,
            value: required
          },
          description: {
            label: Drupal.t('Description'),
            type: 'text',
            default: '',
            value: description
          }
        };

        // Complete the settings object with the field type specific settings.
        $.extend(settings, fieldconfig.settings);

        // Walk each of the settings and check for either actual value overrides or value defaults.
        for (var setting in settings) {
          // Include only field type specific values, skip the common settings.
          if (!fieldconfig.settings.hasOwnProperty(setting)) {
            continue;
          }

          if (settingValues[setting] !== undefined) {
            // There is a actual setting value present, this has priority.
            settings[setting].value = settingValues[setting];
          }
          else if (settings[setting].default != undefined) {
            // There is no setting value present, but a default value exists.
            settings[setting].value = settings[setting].default;
          }
        }

        // Get a themed field element prepared for DOM insertion.
        var field = Drupal.theme(
          'vfcField',
          type,
          label,
          varname,
          settings,
          configID
        );

        // Append the new field element to the target container.
        $container.append(field);

        // Check the "no fields" warning of the target container (= make sure it's hidden actually).
        Drupal.vfc.showHideNoFieldsWarning($container);
      }
      else {
        // No valid field type was given - this is an error.
        if (!fieldtype.length) {
          // The user simply forgot to choose a field type to add - draw his/her attention.
          alert(Drupal.t('Please select a field type to add first!'));
        }
        else {
          // Now that's a bit nastier - the sub-module's api implementation is buggy.
          // We will alert the user with that error in order to enable him to de-activate
          // the sub-module concerned.
          alert(Drupal.t(
            'Field type implementation is faulty! Check sub-module code (field type: !fieldtype).',
            {'!fieldtype': fieldtype}
          ));
        }
      }

      // Reset the fieldtype select after adding the field to prevent multiple clicks.
      $instance.data('$fieldtype_selector').val($('option:first', $instance.data('$fieldtype_selector')).val());
    },

    /**
     * Button Click Callback: deletes a field.
     */
    deleteField: function() {
      // Store the clicked button as a more readable shortcut.
      var $button = $(this);

      // Determine the vfcField element the button belongs to.
      var $field = $($button.parents().filter('.vfcElement').get(0));

      // Determine the field label (if any was set).
      var flabel = $('input[data-type="label"]', $field).val().length
                    ? $('input[data-type="label"]', $field).val()
                    : '<no label>';

      // Ask for permission to actually delete the field.
      if (confirm(Drupal.t('Are you sure to delete the field "!fieldlabel"?', {'!fieldlabel': flabel}))) {
        // Permission was given.

        // Determine the container the field is located in.
        var $container = $field.parent();

        // Remove the field DOM element.
        $field.remove();

        // Check the "no fields" warning inside the container.
        Drupal.vfc.showHideNoFieldsWarning($container);
      }
    },

    /**
     * Button Click Callback: accordion function for container elements.
     *
     * This function takes care of the sliding (accordion) mechanism of vfcElements.
     * It also takes care of displaying appropriate element labels in order to
     * max out the ease of use of the interface.
     */
    showHideContent: function() {
      // Prerequisites: collect a bunch of information about the element concerned.

      // Store the clicked button as a more readable shortcut.
      var $button = $(this);

      // Determine the element about to "slide" - it's the first DOM sibling
      // following the button's parent DOM node.
      var $toSlide = $button.parent().next();

      // Which vfcElement is affected by the slide?
      var $vfcElement = $($toSlide.parents().filter('.vfcElement').get(0));

      // Store a shortcut to the element's header label.
      var $vfcElementHeaderLabel = $('> .vfcElementHeader > label', $vfcElement);

      // Store a shortcut to the label next to the button
      var $slideButtonLabel = $('> label', $button.parent());

      // Determine the type of the container to slide.
      var slideType = $toSlide.attr('data-type');

      // Determine the type of the vfcElement.
      var elementType = $vfcElement.attr('data-type');

      // Finally, determine the actual slide state of the slide element.
      var state = $toSlide.attr('data-state');

      // Now let's do the actual sliding magic...

      // First, distinct between an opened and a closed state.
      switch (state) {

        // These are the open-sliding actions for a closed container.
        case 'closed':
          // First, set the container state to 'opened' because
          // subsequent function calls may check the state prior
          // to the end of the sliding animation created by jQuery.
          $toSlide.attr({ 'data-state': 'opened' });

          // Next, distinct between a plain field element and a group container.
          switch (elementType) {

            // Open-sliding preparation actions for a group container.
            case 'vfcGroup':
              // A group contains more than one slider - in fact, there are three of them.
              //   i) the group container itself
              //  ii) the settings section of a group
              // iii) the field container
              switch (slideType) {

                // "groupContent" is the fields section of a group.
                case 'groupContent':
                  // Remove previously added fields information to the fields section label.
                  $('span', $slideButtonLabel).remove();

                  // As we are about to open the group's fields container,
                  // make this the new active target container.
                  Drupal.vfc.setActiveContainer($vfcElement);
                  break;

                // "elementSettings" is the settings section of a group.
                case 'elementSettings':
                  // Remove previously added group settings information from the group label.
                  $('span', $vfcElementHeaderLabel).remove();
                  break;

                // And last, "elementContent" is the whole group
                // including both the fields and settings section.
                case 'elementContent':
                  // Remove all previously added information from the group label.
                  $('span', $vfcElementHeaderLabel).remove();

                  // Determine if the group's settings section is still closed.
                  var settingsState = $('> .vfcElementContentWrapper > .vfcElementSettingsWrapper > .vfcElementSettings', $vfcElement).attr('data-state');
                  if (settingsState !== undefined && settingsState == 'closed') {
                    // The settings section is still closed, so we add the group label to the element header.

                    // In order to do this, we need to determine the group's actual label value.
                    var groupLabel = $('> .vfcElementSettingsWrapper > .vfcElementSettings input[data-type="groupLabel"]', $toSlide).val();

                    // In case no label was set, we will display a placeholder instead.
                    if (!groupLabel.length) {
                      groupLabel = '&lt;no group label&gt;';
                    }

                    // Append the group's label to the element header.
                    $vfcElementHeaderLabel.append('<span> &quot;' + groupLabel + '&quot;</span>');
                  }

                  // Determine the group's fields section sliding state.
                  var contentState = $('> .vfcElementContentWrapper > .vfcGroupContentWrapper > .vfcContainer', $vfcElement).attr('data-state');;
                  if (contentState === undefined || contentState == 'opened') {
                    // The fields section is opened, so we are able to set this
                    // container as the new active target container.
                    Drupal.vfc.setActiveContainer($vfcElement);
                  }
                  break;
              }
              break;

            // Open-sliding preparation actions for a plain field.
            default:
              // Remove previously added information from the field's header label.
              $('span', $vfcElementHeaderLabel).remove();
              break;
          }

          // Now all presupposed tasks for sliding the element open are done.
          // Now change the button label to the "opened" state...
          $button.text(String.fromCharCode(9650));

          // ...and finally, slide the element to open.
          $toSlide.slideDown();
          break;

        // These are the close-sliding actions for an opened container.
        case 'opened':
        // 'default' will be the case if no prior slide action was called on this container.
        default:

          // First, set the container state to 'closed' because
          // subsequent function calls may check the state prior
          // to the end of the sliding animation created by jQuery.
          $toSlide.attr({ 'data-state': 'closed' });

          // Next, distinct between a plain field element and a group container.
          switch (elementType) {

            // Close-sliding actions for a group container.
            case 'vfcGroup':
              // As we are about to potentially close a group, we need to determine the
              // parent container in order to set this as the new active target.
              // And because two of three slides do affect container selection, this is
              // done here to avoid duplicated code.
              var $parentElement = $($vfcElement.parents().filter('.vfcElement').get(0));

              // A group contains more than one slider - in fact, there are three of them.
              // See the "opening" actions section for more information on that.
              switch (slideType) {

                // "groupContent" id the fields section of a group.
                case 'groupContent':
                  // Count the group's content in order to include that information in the fields section label.
                  var elementCount = $('> .vfcElement', $toSlide).length;

                  // Append the element count to the field section label.
                  $slideButtonLabel.append('<span> (' + elementCount + ' ' + (elementCount == 1 ? Drupal.t('element') : Drupal.t('elements')) + ')</span>');

                  // Now that this container will be closed, set the parent container
                  // as the new active target container.
                  Drupal.vfc.setActiveContainer($parentElement);
                  break;

                // "elementSettings" is the settings section of a group.
                case 'elementSettings':
                  // Remove possibly added information from the element's header label.
                  $('span', $vfcElementHeaderLabel).remove();

                  // Determine the actual group label value.
                  var groupLabel = $('input[data-type="groupLabel"]', $toSlide).val();

                  // If no group label was set, define a placeholder instead.
                  if (!groupLabel.length) {
                    groupLabel = '&lt;no group label&gt;';
                  }

                  // Append the group label to the group header label to ease identification of this group.
                  $vfcElementHeaderLabel.append('<span> &quot;' + groupLabel + '&quot;</span>');
                  break;

                // At last, "elementContent" is the whole group
                // including both the fields and settings section.
                case 'elementContent':
                  // Remove potentionally added information from the element's header label.
                  $('span', $vfcElementHeaderLabel).remove();

                  // Determine the actual group label value.
                  var groupLabel = $('input[data-type="groupLabel"]', $toSlide).val();

                  // If no group label was set, define a placeholder instead.
                  if (!groupLabel.length) {
                    groupLabel = '&lt;no group label&gt;';
                  }

                  // Count the group's content in order to include that information in the fields section label.
                  var elementCount = $('> .vfcGroupContentWrapper > .vfcContainer > .vfcElement', $toSlide).length;

                  // Append the group label as well as the element count to the group's header label.
                  $vfcElementHeaderLabel.append('<span> &quot;' + groupLabel + '&quot; (' + elementCount + ' ' + (elementCount == 1 ? Drupal.t('element') : Drupal.t('elements')) + ')</span>');

                  // Now that this container will be closed, set the parent container
                  // as the new active target container.
                  Drupal.vfc.setActiveContainer($parentElement);
                  break;
              }
              break;

            // Close-sliding actions for a plain field.
            default:
              // Determine the field's label value.
              var label = $('> .vfcElementContentWrapper .vfcElementSettings input[data-type="label"]', $vfcElement).val();

              // If no field label was set, define a placeholder instead.
              if (!label.length) {
                label = '&lt;no label&gt;'
              }

              // Determine the field's variable name.
              var varname = $('> .vfcElementContentWrapper .vfcElementSettings input[data-type="varname"]', $vfcElement).val();

              // If no variable name was set, define a placeholder instead.
              if (!varname.length) {
                varname = '&lt;no variable name&gt;';
              }

              // Append the field's label and variable name to the element's header label.
              $vfcElementHeaderLabel.append('<span> (' + label + ' / ' + varname + ')</span>');
              break;
          }

          // All presupposed tasks for sliding the element to closed state are done.
          // Now change the button label to the "closed" state...
          $button.text(String.fromCharCode(9660));

          // ...and finally, close the element.
          $toSlide.slideUp();
          break;
      }
    },

    /**
     * Helper function: determine active container ($root_container is fallback).
     *
     * @returns {HTMLElement}
     */
    getActiveContainer: function($instance) {
      var active = $('.vfcActive', $instance.data('$root_container'));
      return active.length ? active : $instance.data('$root_container');
    },

    /**
     * Helper function: set active container.
     *
     * @param {HTMLElement} $container
     */
    setActiveContainer: function($container) {
      // determine the current instance
      var $config_area = $($container.parents().filter('#vfcAdminArea').get(0));
      var $instance = $config_area.data('instance');

      // Remove the actual "active" marker.
      Drupal.vfc.getActiveContainer($instance).removeClass('vfcActive');

      // Because of a slightly different DOM tree we need to distinct between
      // the root container and other, "normal" group containers.
      $container = $container.parent().attr('id') == 'vfcAdminArea'
                    ? $instance.data('$root_container')
                    : $('> .vfcElementContentWrapper > .vfcGroupContentWrapper > .vfcContainer', $container);

      // Now that the container is properly determined, let's see if it is
      // opened or closed. It just don't makes sense to activate a closed
      // container, so activate the parent one instead in these cases.
      // But only do the fallback if it is NOT the root container... ;-)
      if ($container.parent().attr('id') != 'vfcAdminArea') {
        // First we need to properly determine the vfcGroup element.
        var $group = $($container.parents().filter('.vfcElement').get(0));

        // Next, we check if the group itself is opened.
        var groupState = $('> .vfcElementContentWrapper', $group).attr('data-state') != undefined
                          ? $('> .vfcElementContentWrapper', $group).attr('data-state')
                          : 'opened';

        // Secondly, is the fields section of the group also opened?
        var containerState = $container.attr('data-state') != undefined
                              ? $container.attr('data-state')
                              : 'opened';

        // If one of these two are NOT opened, we will do the fallback to the parent container.
        if (!(groupState == 'opened' && containerState == 'opened')) {
          // The container is actually closed - switch to it's parent.
          $container = $($container.parents().filter('.vfcContainer').get(0));
        }
      }

      // Now we have a opened, usable container to set as an active target.
      $container.addClass('vfcActive');
    },

    /**
     * Helper function: shows or hides the "no fields" warning.
     *
     * @param {HTMLElement} $container (optional)
     */
    showHideNoFieldsWarning: function($container) {
      if ($('> .vfcElement', $container).length > 0) {
        $('> .noFields', $container).hide();
      }
      else {
        $('> .noFields', $container).show();
      }
    },

    /**
     * JavaScript form submission handler.
     *
     * This is called upon clicking the "save configuration" button. It will
     * call the configuration validation and - after successful validation -
     * set the serialized field configuration to Drupal's "real" FAPI field.
     *
     * @param {Object} e
     */
    checkConfiguration: function(e) {
      var $instance = $(this).data('instance');

      // Remove all previous error markings.
      $('*', $instance.data('$root_container')).removeClass('vfcError');

      // Remove all previous messages.
      $instance.data('$message_area').empty().hide();

      // Call the validator - this will recursively validate all
      // elements configured and also build up the configuration object.
      var validationRun = Drupal.vfc.buildConfiguration($instance.data('$root_container'), 0);

      // Shortcut to the validation result object.
      var validation = validationRun.validation;

      // The calculated configuration depth.
      var depth = validationRun.depth;

      // The property "success" is a boolean flag indicating
      // if the configuration made is valid.
      if (validation.success) {
        // All fine - serialize the configuration array.
        var config = JSON.stringify({
          fields: validation.config,
          depth: depth
        });

        // Write the config to Drupal's FAPI field.
        $instance.val(config);
      }
      else {
        // Sort the error messages.
        validation.errors.sort();

        // Only display unique error messages.
        var errorDisplay = [];
        var lastError = false;
        for (var index in validation.errors) {
          if (validation.errors[index] !== lastError) {
            errorDisplay.push(validation.errors[index]);
            lastError = validation.errors[index];
          }
        }

        // Configuration validation failed - set error messages.
        $instance.data('$message_area').html('<p>' + errorDisplay.join('</p><p>') + '</p>').show();

        // Prevent form submission.
        e.preventDefault();

        // Throw a warning.
        alert(Drupal.t('Please check your configuration!'));
      }
    },

    /**
     * Builds and checks the virtual field collection configuration.
     *
     * This function iterates recursively through all configured groups
     * and fields of this virtual field collection ensuring all settings
     * made are production safe.
     *
     * @param {HTMLElement} $root (optional)
     * @param {int} depth (optional)
     * @returns {object}
     */
    buildConfiguration: function($root, depth) {
      // Result object declaration for the current run.
      var result = {
        // We estimate a good result and will change this flag if we stumble upon an error.
        success: true,
        noFields: false,
        config: {},
        errors: [],
        varnames: []
      };

      // Set the actual depth as a local variable in order to make it
      // accessible for subsequent recursive calls.
      var level = depth;

      // This is a regular expression prepared to test variable names
      // for disallowed characters. Allowed characters are a-z and 0-9
      // without any spaces or punctuation and have to be at least
      // 3 characters long.
      var allowedVariableNames = /^[a-z0-9]{3,}$/i;
      var varnameSelector = '> .vfcElementContentWrapper > .vfcElementSettingsWrapper input[data-type="varname"]';
      var varnameErrors = {
        tooShort: Drupal.t('Variable names have to be at least 3 characters long.'),
        disallowed: Drupal.t('The variable name chosen contains disallowed characters. Please do only use letters and numbers.')
      };

      // Check each contained element...
      $('> .vfcElement', $root).each(function(index, elem) {

        // Element is a group of fields / groups.
        if ($(elem).attr('data-type') == 'vfcGroup') {
          // Determine if the group already has a unique ID.
          var configID = $(elem).attr('data-configid');

          // Generate a new ID if no previous ID is present.
          if (configID === undefined) {
            configID = Drupal.vfc_helper.generateUniqueID();
          }

          // Determine the variable name assigned to this group.
          var varname = $(varnameSelector, elem).val();

          // Validate the variable name.
          if (!varname.length) {
            // No variable name was set - this is an error.

            // Set an appropriate error message.
            result.errors.push(Drupal.t('Missing variable name on field group.'));

            // Mark this validation run as failed.
            result.success = false;

            // Mark this element's variable name input as faulty.
            $(varnameSelector, elem).addClass('vfcError');

            // Mark the element itself as faulty.
            $(elem).addClass('vfcError');
          }
          else if (!(allowedVariableNames.test(varname))) {
            // The variable name chosen contains disallowed characters.

            // Set an appropriate error message.
            if (varname.length < 3) {
              result.errors.push(varnameErrors.tooShort);
            }
            else {
              result.errors.push(varnameErrors.disallowed);
            }

            // Mark this validation run as failed.
            result.success = false;

            // Mark this element's variable name input as faulty.
            $(varnameSelector, elem).addClass('vfcError');

            // Mark the element itself as faulty.
            $(elem).addClass('vfcError');
          }
          else if (result.varnames.indexOf(varname) !== -1) {
            // This variable name is already used on this level. This
            // is an error because the value of this element will overwrite
            // the previous value defined.

            // Mark this validation run as failed.
            result.success = false;

            // Mark this element's variable name input as faulty.
            $(varnameSelector, elem).addClass('vfcError');

            // Mark the element itself as faulty.
            $(elem).addClass('vfcError');

            // Set an apropriate error message.
            result.errors.push(Drupal.t('Duplicate variable name: "!varname".', {'!varname': varname}));
          }
          else {
            // The variable name passed all tests - keep track of it.
            result.varnames.push(varname);
          }

          // Actual element is a group - recurse on it's content.
          var subValidationRun = Drupal.vfc.buildConfiguration(
            $('> .vfcElementContentWrapper > .vfcGroupContentWrapper > .vfcContainer', elem),
            level + 1
          );

          // Shortcut to the sub-validation result.
          var subresult = subValidationRun.validation;

          // Track the maximum depth of the actual configuration.
          if (subValidationRun.depth > level) {
            level = subValidationRun.depth;
          }

          // Elements contained in this group did not validate...
          if (!subresult.success) {
            // Bubble up the error flag.
            result.success = false;

            // Bubble up the error messages.
            result.errors = result.errors.concat(subresult.errors);

            // Mark this element as faulty.
            $(elem).addClass('vfcError');
          }

          // DOM structure to the settings elements
          var settingsSelector = '> .vfcElementContentWrapper > .vfcElementSettingsWrapper input';

          // Build up the group object regardless of the validation outcome
          // in order to complete the validation run.
          var group = {
            type: 'group',
            configID: configID,
            groupLabel: $(settingsSelector + '[data-type="groupLabel"]', elem).val(),
            buttonLabel: $(settingsSelector + '[data-type="buttonLabel"]', elem).val(),
            minEntries: $(settingsSelector + '[data-type="minEntries"]', elem).val(),
            maxEntries: $(settingsSelector + '[data-type="maxEntries"]', elem).val(),
            groupLabelField: $(settingsSelector + '[data-type="groupLabelField"]', elem).attr('checked'),
            noFields: subresult.noFields,
            fields: subresult.config
          };

          // Add the group and it's content to the configuration result object.
          result.config[varname] = group;
        }

        // Element is a plain field.
        else {
          // Determine if the field already has a unique ID.
          var configID = $(elem).attr('data-configid');

          // Generate a new ID if no previous ID is present.
          if (configID === undefined) {
            configID = Drupal.vfc_helper.generateUniqueID();
          }

          // Determine the variable name of the field.
          var varname = $('> .vfcElementContentWrapper input[data-type="varname"]', elem).val();

          // Actual element is a single field - build the field configuration object.
          var field = {
            type: $(elem).attr('data-type'),
            label: $('> .vfcElementContentWrapper input[data-type="label"]', elem).val(),
            required: $('> .vfcElementContentWrapper input[data-type="required"]', elem).attr('checked'),
            description: $('> .vfcElementContentWrapper input[data-type="description"]', elem).val(),
            settings: {},
            configID: configID
          };

          // Keep track of used variable names.
          if (varname.length && result.varnames.indexOf(varname) === -1) {

            // The actual variable name was not used previously on this level - check it.
            if (!(allowedVariableNames.test(varname))) {
              // The variable name chosen contains disallowed characters.

              // Set an appropriate error message.
              if (varname.length < 3) {
                result.errors.push(varnameErrors.tooShort);
              }
              else {
                result.errors.push(varnameErrors.disallowed);
              }

              // Mark this validation run as failed.
              result.success = false;

              // Mark this element's variable name input as faulty.
              $(varnameSelector, elem).addClass('vfcError');

              // Mark the element itself as faulty.
              $(elem).addClass('vfcError');
            }
            else {
              // All fine, keep track of this varname.
              result.varnames.push(field.varname);
            }
          }
          else {
            // The actual variable name was either not set or already used on this level.
            // This is an error because values would override previous values.

            // Mark this validation run as failed.
            result.success = false;

            // Mark this element's variable name input as faulty.
            $('> .vfcElementContentWrapper input[data-type="varname"]', elem).addClass('vfcError');

            // Mark the element itself as faulty.
            $(elem).addClass('vfcError');

            // Set an apropriate error message.
            if (!varname.length) {
              result.errors.push(Drupal.t('Missing variable name on field type !type.', {'!type': field.type}));
            }
            else {
              result.errors.push(Drupal.t('Duplicate variable name: "!name".', {'!name': varname}));
            }
          }

          // Check for a given field label.
          if (!field.label.length) {
            // No field label was set, but this is a required setting.

            // Mark this validation run as failed.
            result.success = false;

            // Set an appropriate error message.
            result.errors.push(Drupal.t(
              'Field "!varname" (!type) without label.',
              {
                '!varname': (varname ? varname : '&lt;' + Drupal.t('no variable name') + '&gt;'),
                '!type': field.type
              }
            ));

            // Mark this element's label input as faulty.
            $('> .vfcElementContentWrapper input[data-type="label"]', elem).addClass('vfcError');

            // Mark the element itself as faulty.
            $(elem).addClass('vfcError');
          }

          // Fetch the field definition as defined by the sub-module.
          var definition = Drupal.vfc.fetchFieldConfig(field.type).settings;

          // Iterate over each single setting defined.
          for (var setting in definition) {

            // Shortcut to the field type for better readability.
            var type = definition[setting].type;

            // The jQuery selector string to the setting's input element.
            var settingSelector = '> .vfcElementContentWrapper > .vfcElementSettingsWrapper (input,select)[data-type="' + setting + '"]';

            // Distinct between setting types to fetch correct values.
            switch (type) {

              // 'set' is a set of checkboxes and may contain multiple values.
              case 'set':
                var value = [];

                // Walk each option.
                $(settingSelector, elem).each(function() {
                  // If this option is activated, track it.
                  if ($(this).attr('checked')) {
                    value.push($(this).val());
                  }
                });
                break;

              // Booleans are single-value settings with a checkbox.
              case 'bool':
                var value = $(settingSelector, elem).attr('checked')
                              ? true
                              : false;
                break;

              // Ints have to be numbers...
              case 'int':
                var value = Number($(settingSelector, elem).val());
                break;

              // Select fields do have a slighty different way to determine the value.
              case 'select':
                var value = $(settingSelector + ' option:selected', elem).val();
                break;

              // All others are single-value types.
              default:
                var value = $(settingSelector, elem).val();
                break;
            }

            // Push the setting's value to the field configuration object.
            field.settings[setting] = value;
          }

          // Push the field to the result config.
          result.config[varname] = field;

          // Now all configuration data for this field is collected...

          // Determine the sub-module configuration validation callback.
          var validationCallback = Drupal.vfc.fieldapi[field.type].checkConfiguration;

          // Try to call the sub-module config validation.
          try {
            var validConfig = validationCallback(field, definition, $(elem));

            // The vfc field api expects TRUE if the validation succeeded.
            // All other return values will be assumed a failed validation.
            if (validConfig !== true) {
              // Configuration is faulty - save the error message.
              result.errors.push(validConfig);

              // Mark this validation run as failed.
              result.success = false;

              // Ensure the element is marked as faulty.
              $(elem).addClass('vfcError');
            }
          } catch (e) {
            // Faulty sub-module implementation - throw a warning in order to
            // enable the user to de-activate the sub-module.
            alert(Drupal.t('Faulty sub-module implementation of callback checkConfiguration for field type !type', {'!type': field.type}));

            // Catch the exception message for debugging purposes.
            result.errors.push(e.message);

            // Because the sub-module failed validating it's own field, we cannot
            // assume the actual configuration as production safe - flag the
            // validation run as failed.
            result.success = false;
          }
        }
      });

      // If no fields at all were found, this is an error.
      if (!Drupal.vfc_helper.getObjectLength(result.config)) {
        result.success = false;
        result.noFields = true;
        result.errors.push(Drupal.t('No fields defined!'));
      }

      // Return the validation result and the maximum depth.
      return {validation: result, depth: level};
    },

    /**
     * Object placeholder for sub-module api implementations.
     */
    fieldapi: {},

    /**
     * A collection of theme hooks used by this interface.
     */
    theme: {
      /**
       * Themes an interface button.
       *
       * @param {string} callback
       *        function callback for the button
       * @param {string} label
       *        text for the button label
       * @param {array} attributes (optional)
       *        additional html attributes for the button tag
       * @returns {string}
       */
      vfcButton: function(callback, label) {
        return '<button class="vfcButton" data-action="' + callback + '">' + label + '</button>';
      },

      /**
       * Themes a settings widget.
       *
       * @param {object} settings
       *        an object of objects for the element settings
       *        - key: name of the setting
       *        - value: {label, type, [options], default, [value]}
       * @param {boolean} showAccordionButton
       * @returns {string}
       */
      vfcElementSettings: function(settings, showAccordionButton) {
        if (settings === undefined || typeof settings !== 'object') {
          return '';
        }
        else {
          if (showAccordionButton === undefined) {
            showAccordionButton = false;
          }
          // Header of the setting widget.
          var settingsWidget = '<div class="vfcElementSettingsWrapper">' +
                                 '<div class="vfcElementHeader vfcElementSubHeader">' +
                                   (showAccordionButton ? Drupal.theme('vfcButton', 'showHideContent', '&#9650;') : '') +
                                   '<label>&nbsp;&nbsp;' + Drupal.t('Settings') + '</label>' +
                                 '</div><div class="vfcElementSettings" data-type="elementSettings">' +
                                   '<table><thead><tr>' +
                                     '<th class="vfcSettingLabel">' + Drupal.t('Setting') + '</th>' +
                                     '<th>' + Drupal.t('Value') + '</th>' +
                                   '</tr></thead><tbody>';

          // The settings one by one.
          for (var setting in settings) {
            // Opening the markup for the current setting.
            var settingRequired = settings[setting].required !== undefined && settings[setting].required;
            settingsWidget += '<tr><td class="vfcSettingLabel">' +
                              Drupal.t(settings[setting].label) +
                              (settingRequired ? Drupal.vfc.form_required_markup : '') +
                              '</td><td>';

            // The setting's value.
            var settingValue = settings[setting].value !== undefined
                                ? settings[setting].value
                                : (settings[setting].default !== undefined
                                    ? settings[setting].default
                                    : (settings[setting].type == 'set' ? [] : false)
                                  );

            // The input element(s).
            settingsWidget += Drupal.theme(
              'vfcInputElement',
              settings[setting].type,
              setting,
              settingValue,
              settings[setting].field_prefix !== undefined ? settings[setting].field_prefix : '',
              settings[setting].field_suffix !== undefined ? settings[setting].field_suffix : '',
              (settings[setting].options !== undefined ? settings[setting].options : null)
            );

            // Closing the markup for the current setting.
            settingsWidget += '</td></tr>';
          }

          // Footer of the settings widget.
          settingsWidget += '</tbody></table></div></div>';

          // Return the widget markup.
          return settingsWidget;
        }
      },

      /**
       * Themes a vfc input element.
       *
       * Note:   These form elements intentionally do NOT use a "name" attribute!
       * Reason: Since these are "virtual" fields unknown to the drupal backend there is
       *         no need to post them back to the server. Input elements without a name
       *         attribute are simply ignored by the browser on form submission.
       *
       * @param {string} type
       * @param {string} name
       * @param {*} value
       * @param {string} field_prefix
       * @param {string} field_suffix
       * @param {Array} options (optional)
       * @returns {string}
       */
      vfcInputElement: function(type, name, value, field_prefix, field_suffix, options) {
        if (options === undefined || typeof options !== 'object') {
          options = {};
        }
        var element;
        switch (type) {
          case 'text':
          case 'int':
            if (value === undefined || value == null) {
              value = false;
            }
            element = field_prefix;
            element += '<input type="text" data-type="' + name + '" class="form-text" data-type="' + type + '"';
            if (value) {
              element += ' value="' + (type === 'int' && value.length ? Number(value) : value) + '"';
            }
            element += '/>';
            element += field_suffix;
            break;

          case 'bool':
            element = field_prefix;
            element += '<input type="checkbox" data-type="' + name + '" value="1"';
            if (value === true) {
              element += ' checked="checked"';
            }
            element += '/>';
            element += field_suffix;
            break;

          case 'set':
            element = field_prefix;;
            for (var option in options) {
              element += '<div><input type="checkbox" data-type="' + name + '" value="' + option + '"';
              if (value.indexOf(option) != -1) {
                element += ' checked="checked"';
              }
              element += '/> ' + options[option] + '</div>';
            }
            element += field_suffix;
            break;

          case 'select':
            element = field_prefix;
            element += '<select data-type="' + name + '"><option value="">' +
                      Drupal.t('Please select') +
                      '</option>';
            for (var option in options) {
              element += '<option value="' + option + '"';
              if (value === option) {
                element += ' selected="selected"';
              }
              element += '> ' + options[option] + '</option>';
            }
            element += '</select>';
            element += field_suffix;
            break;
        }
        return element;
      },

      /**
       * Themes a vfc element container.
       *
       * @param {string} type
       *        the type of the element
       * @param {string} title
       *        the title for the element header
       * @param {Array} buttons (optional)
       *        buttons to place in the header
       * @param {Array} settings (optional)
       *        array of element settings
       * @param {string} content (optional)
       *        content of the element
       * @param {string} footer (optional)
       *        content of the footer
       * @param {Object} attributes (optional)
       *        additional html tag attributes
       * @returns {string}
       */
      vfcElement: function(type, title, buttons, settings, content, footer, attributes) {
        // Default values for optional arguments.
        if (buttons === undefined || typeof buttons !== 'object') {
          buttons = '';
        }
        else {
          buttons = buttons.join('');
        }
        if (settings === undefined || typeof settings !== 'object') {
          settings = {};
        }
        if (content === undefined) {
          content = '';
        }
        if (footer === undefined) {
          footer = '';
        }
        else {
          footer = '<div class="vfcElementFooter">' + footer + '</div>';
        }
        if (attributes === undefined || typeof attributes !== 'object') {
          attributes = {};
        }

        // Construct the attribute string.
        var attributeString = [];
        for (var attribute in attributes) {
          // Prevent doubling the "data-type" attribute.
          if (attribute == 'data-type') {
            continue;
          }
          // Ignore undefined attribute values.
          if (attributes[attribute] === undefined) {
            continue;
          }
          var attributeName = attribute.replace('"', '');
          var attributeValue = attributes[attribute].replace('"', '');
          attributeString.push(attributeName + '="' + attributeValue + '"');
        }
        attributeString = attributeString.length
                           ? ' ' + attributeString.join(' ')
                           : '';

        // Construct the element.
        var vfcElement = '<div class="vfcElement" data-type="' + type + '"' + attributeString + '>' +
                           '<div class="vfcElementHeader">' +
                             buttons +
                             '<label>&nbsp;&nbsp;' +
                               title +
                         '</label></div><div class="vfcElementContentWrapper" data-type="elementContent">' +
                             Drupal.theme('vfcElementSettings', settings, type == 'vfcGroup') +
                             content +
                             footer +
                         '</div></div>';
        return vfcElement;
      },

      /**
       * Themes a group container.
       *
       * @param {string} groupLabel (optional)
       * @param {string} varname (optional)
       * @param {string} buttonLabel (optional)
       * @param {int} minEntries (optional)
       * @param {int} maxEntries (optional)
       * @param {boolean} groupLabelField (optional)
       * @param {string} configID (optional)
       * @returns {string}
       */
      vfcGroup: function(groupLabel, varname, buttonLabel, minEntries, maxEntries, groupLabelField, configID) {
        // Default values for function arguments.
        if (groupLabel === undefined) {
          groupLabel = '';
          varname = '';
          buttonLabel = '';
          minEntries = 0;
          maxEntries = '';
          groupLabelField = false;
          configID = Drupal.vfc_helper.generateUniqueID();
        }

        // Buttons for the element header.
        var buttons = [
          Drupal.theme('vfcButton', 'deleteGroup', '&times;'),
          Drupal.theme('vfcButton', 'showHideContent', '&#9650;')
        ];

        // Settings for the group.
        var settings = {
          groupLabel: {
            label: Drupal.t('Label') + Drupal.vfc.form_required_markup,
            type: 'text',
            default: '',
            value: groupLabel
          },
          varname: {
            label: Drupal.t('Variable name') + Drupal.vfc.form_required_markup,
            type: 'text',
            default: '',
            value: varname
          },
          buttonLabel: {
            label: Drupal.t('Button-Label') + Drupal.vfc.form_required_markup,
            type: 'text',
            default: '',
            value: buttonLabel
          },
          minEntries: {
            label: 'Minimum entries required',
            type: 'int',
            default: 0,
            value: minEntries
          },
          maxEntries: {
            label: 'Maximum entries allowed',
            type: 'int',
            default: '',
            value: maxEntries
          },
          groupLabelField: {
            label: 'Provide Grouplabel-Field',
            type: 'bool',
            default: false,
            value: groupLabelField
          }
        };

        // The field container markup as content for the element.
        var fieldContainer = '<div class="vfcGroupContentWrapper">' +
                               '<div class="vfcElementHeader vfcElementSubHeader">' +
                                 Drupal.theme('vfcButton', 'showHideContent', '&#9650;') +
                                 '<label>&nbsp;&nbsp;' + Drupal.t('Fields') + '</label>' +
                               '</div><div class="vfcContainer" data-type="groupContent">' +
                               Drupal.vfc.group_empty_markup +
                               '</div></div>';

        // Theme the element and return it.
        return Drupal.theme(
          'vfcElement',
          'vfcGroup',
          Drupal.t('Group'),
          buttons,
          settings,
          fieldContainer,
          undefined,
          {'data-configid': configID}
        );
      },

      /**
       * Themes a field container.
       *
       * @param {string} type
       * @param {string} label
       * @param {string} varname
       * @param {object} settings
       * @param {string} configID
       * @returns {string}
       */
      vfcField: function(type, label, varname, settings, configID) {
        // Buttons for the element header.
        var buttons = [
          Drupal.theme('vfcButton', 'deleteField', '&times;'),
          Drupal.theme('vfcButton', 'showHideContent', '&#9650;')
        ];

        // Human readable type name.
        var typelabel = Drupal.vfc.fetchFieldConfig(type).label;

        // Theme the element and return it.
        return Drupal.theme(
          'vfcElement',
          type,
          Drupal.t(typelabel),
          buttons,
          settings,
          undefined,
          undefined,
          {'data-configid': configID}
        );
      }
    }
  };

  /**
   * Startup functions.
   */
  Drupal.behaviors.virtual_field_collection = {
    attach: function (context, settings) {
      // Extend Drupal with our own js theme hooks.
      $.extend(Drupal.theme.prototype, Drupal.vfc.theme);

      // initialize the interface
      $('textarea.vfc_serialized_data').once(function(index, field){
        Drupal.vfc.initInterface(field);
      });
    }
  }
}(jQuery));
