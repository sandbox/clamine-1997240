/**
 * @file
 * Contains the javascript implementation for the virtual field collection implementation
 * interface.
 */

/**
 * Function library for the field collection implementation.
 */
(function ($) {
  Drupal.vfc = {

    // Preparation of some local variables.
    form_required_markup: ' <span class="form-required" title="' + Drupal.t('This field is required.') + '">*</span>',
    xhr_requests: {},
    queued_operations: { queueID: 0 },

    /**
     * Cache some DOM elements to speed things up.
     *
     * This implementation is a bit trickier than the one on the configuration
     * interface because there can be more than one instance of a vfc field.
     * So we have to cleanly separate the related DOM elements for all instances.
     */
    cacheDOMElements: function($instance) {
      // Determine the field's name value.
      var name = $instance.attr('name');

      // As we only store the plain field name as the key within the
      // javascript settings object, we need to be sure that we get rid
      // of all language- and key-identifier-parts Drupal core added to the
      // form element (e.g. fieldname[und][value]. This should generally be
      // the case, but we'll be on the safe side if we check it.
      $instance.data('name', name.indexOf('[') !== -1 ? name.split('[').shift() : name);

      // Next, we have to locate the vfc admin area for that field. We should
      // be able to achieve that by searching the parent DOM elements of the
      // field's instance for a div wrapper element with the css class
      // "field-type-vfc-serialized-data". However, it MAY be the case that
      // some custom themes alter the html structure of forms. If this is the
      // case, we will fail gloriously on this part, but this is something that
      // can't be taken into account. The only thing we will do in these
      // (hopefully seldom) cases is to inform the user about it and suggest
      // to switch the active theme to a more standard compliant one.
      var $fieldWrapper = $($instance.parents().filter('.field-type-vfc-serialized-data').get(0));
      if ($fieldWrapper.length) {

        var $form = $($instance.parents().filter('form').get(0));
        var $config_area = $('.vfcAdminArea', $fieldWrapper);

        // Cache related DOM elements for this instance
        $instance.data({
          '$form': $form,
          '$config_area': $config_area,
          '$message_area': $('.vfcMessages', $fieldWrapper),
          '$root_container': $('.vfcRoot', $fieldWrapper)
        });

        // Also, this is a good opportunity to add an identifier string
        // to both the field instance and the config area to clarify and
        // ease the later access to these elements.
        $instance.attr({'data-instancename': $instance.data('name')});
        $config_area.attr({'data-instancename': $instance.data('name')});

        // Add this instance to it's form
        if ($form.data('vfc_instances') === undefined) {
          $form.data('vfc_instances', []);
        }
        var instances = $form.data('vfc_instances');
        instances.push($instance);
        $form.data('vfc_instances', instances);
      }
      else {
        // Now that's a pitty. We were not able to properly determine the wrapper
        // and therefore we can't state where our admin area is.
        alert(Drupal.t(
          'FATAL ERROR: ' +
          'Virtual field collection was not able to determine the html structure ' +
          'of your active theme. It seems your theme is not standard compliant. ' +
          'However, VFC depends on a certain html structure and is not able to ' +
          'operate properly on the active theme.'
        ));

        // Break further execution of the interface initialization.
        return false;
      }

      // Seems all went fine.
      return true;
    },

    /**
     * Initially called to build up the edit interface(s).
     */
    initInterface: function(field) {
      // Shortcut to the DOM element.
      var $instance = $(field);

      // One-time shot to cache the VFC DOM elements.
      if (!Drupal.vfc.cacheDOMElements($instance)) {
        // Something went wrong, VFC is not able to continue.
        // An error warning was already thrown by cacheDOMElements -
        // we just need to exit further initialization here.
        return false;
      }

      // Bind the vfc submit handler to form submission.
      // We have just to make sure that each form only gets a single
      // submit handler.
      $instance.data('$form').once(function(index, form) {
        $(form).bind('submit', Drupal.vfc.saveFieldValues);
      });

      // Hide the "no JS" warning.
      $('.noJS', $instance.data('$config_area')).remove();

      // In order to properly mark form validation errors on empty
      // fields every element has to have an instance id, starting
      // at the root container. An instance id is just a unique,
      // 32 character long string set as the value to the data-instanceid
      // html attribute which will change on every interface generation.
      // Set the instance id on the root container for later form validation.
      $instance.data('$root_container').attr({
        'data-instanceid': Drupal.vfc_helper.generateUniqueID()
      });

      // Initialize the edit form for the current instance.
      Drupal.vfc.initConfiguration(
        Drupal.settings.virtual_field_collection.fields[$instance.data('name')].config,
        Drupal.settings.virtual_field_collection.fields[$instance.data('name')].data,
        $instance.data('$root_container')
      );

      // Initialize click events.
      Drupal.vfc.bindEvents($instance.data('$root_container'));
    },

    /**
     * Initializes the edit interface for a specific vfc field instance.
     */
    initConfiguration: function(config, data, $root) {
      // Iterate over each configured configuration entry.
      for (var varname in config) {
        // Shortcut to the current element's configuration.
        var elementConfig = config[varname];

        // Shortcut to the element type for better readability.
        var type = elementConfig.type;

        // Distinct the behaviour based on the element's type.
        switch (type) {

          // The element to add is a group.
          case 'group':
            // Add the group container (entry wrapper and "add new entry" button)
            // to the DOM structure in order to create the fields inside.
            var $group = Drupal.vfc.addGroup(elementConfig, $root);

            // Determine if this group contains any data.
            if (data[varname] !== undefined) {
              var groupData = data[varname];
            }
            else {
              var groupData = [];
            }

            // Prepare a local array for the grouplabels.
            var groupEntryLabels = [];

            // If data exists for this element, determine the number of entries
            // contained in the data. If no data exists, determine the minimum
            // number of entries required by the group's configuration settings.
            if (groupData.length) {
              // Extract the stored group entry labels.
              groupEntryLabels = groupData.shift();

              // Count the length of the group entries.
              var entriesToCreate = groupData.length;
            }
            else {
              // Determine the minimum number of configuration given group entries.
              var entriesToCreate = Number(elementConfig.minEntries);
              if (entriesToCreate === 0) {
                entriesToCreate = 1;
              }
            }

            // Add the required number of group entries to this group - at least 1.
            for (var entryNo = 0; entryNo < entriesToCreate; entryNo++) {

              // Try to fetch a possible group label value.
              var groupLabel = groupEntryLabels[entryNo] !== undefined
                                ? groupEntryLabels[entryNo]
                                : '';

              // Get a themed group entry wrapper.
              var entryWrapper = Drupal.theme(
                'vfcGroupEntryWrapper',
                elementConfig,
                groupLabel
              );

              // Append the entry wrapper to the group.
              var $entryWrapper = $(entryWrapper).appendTo($group);

              // Determine the container element in this entry wrapper.
              var $container = $('> .vfcContainer', $entryWrapper);

              // Create a dummy entry data object if necessary.
              if (data[varname] === undefined) {
                var entryData = {};
              }
              else {
                var entryData = groupData[entryNo];
              }

              // Initialize the default sub-entries on this entry container.
              Drupal.vfc.initConfiguration(elementConfig.fields, entryData, $container);
            }
            break;

          // The element to add is a single field.
          default:
            // Create a dummy entry data object if necessary.
            if (data[varname] === undefined) {
              var elementData = null;
            }
            else {
              var elementData = data[varname];
            }

            // Add the field element to the DOM structure.
            var $field = Drupal.vfc.addField(
              varname,
              elementConfig,
              elementData,
              $root
            );

            // Now that the element is created and inserted to the DOM tree,
            // try to call the vfc callback "afterBuild" on the sub-module.
            try {
              Drupal.vfc.fieldapi[elementConfig.type].afterBuild($field, elementConfig);
            } catch (e) {
              if (Drupal.vfc.fieldapi[elementConfig.type].afterBuild === undefined) {
                // The sub-module lacks providing a mandatory callback. But this is not
                // considered a fatal error and we may proceed with the form initialization.
                // Just to keep our developers informed we will dispatch a "silent error".
                console.log(Drupal.t(
                  'Sub-Module providing field type "!fieldtype" does not provide a afterBuild callback.',
                  {'!fieldtype': elementConfig.type}
                ));
              }
              else {
                // The callback exists, but failed on execution. This is also considered a
                // minor bug, and we will also only dispatch a "silent error".
                console.log(Drupal.t(
                  'Callback "afterBuild" failed on field type "!fieldtype".',
                  {'!fieldtype': elementConfig.type}
                ));
              }
            }
            break;
        }
      }
    },

    /**
     * Adds a group container for repeatable fields to the DOM tree.
     */
    addGroup: function(config, $container) {
      // Get a themed group wrapper.
      var group = Drupal.theme('vfcGroup', config);

      // Insert the wrapper to the document DOM.
      var $group = $(group).appendTo($container);

      // Return the field container inside the wrapper.
      return $('> .vfcContainer', $group);
    },

    /**
     * Adds a single field to the DOM tree.
     */
    addField: function(varname, config, data, $container) {
      // Get a themed field element.
      var fieldContainer = Drupal.theme('vfcField', varname, config, data);

      // Insert the element into the document DOM tree.
      var $vfcElement = $(fieldContainer).appendTo($container);

      // Determine the form input elements of this field.
      var $field = $('> .vfcElementContent > .formElement > *', $vfcElement);

      // Return the input elements.
      return $field;
    },

    /**
     * Bind all elements with events to their respective event handler.
     */
    bindEvents: function($root) {
      // Bind all buttons to the event dispatcher.
      $('button.vfcButton', $root)
        .unbind('click')
        .bind('click', function(e) {
          e.preventDefault();
          e.stopPropagation();
          Drupal.vfc.buttonClick($(this), e);
        });

      // Initialize the sorting ability.
      $('.vfcGroupEntries', $root).sortable({
        handle: '.vfcGroupEntryHeader',
        containment: 'parent',
        axis: 'y'
      });

      // Initialize autocomplete fields.
      $('.vfcAutocomplete').bind('click', function() {
        // We'll use the "click" event to select the displayed
        // value in the textfield for easier replacement.
        var $input = $(this);
        $input.select();
      }).bind('keydown', function() {
        var $input = $(this);
        $input.removeAttr('data-value');
      }).bind('blur', function() {
        // We'll use the "blur" event to check if the user actually
        // selected an autocomplete suggestion. If this is NOT the
        // case, the html attribute "data-value" will be missing on
        // the form element and we're safe to remove any data displayed.
        // We do this to make sure the user notices that there is no
        // valid item selected for this field.
        var $input = $(this);
        $input.removeClass('throbbing');
        if ($input.attr('data-value') === undefined) {
          $input.val('');
        }
      }).autocomplete({
        delay: Drupal.settings.virtual_field_collection.config.autocomplete.delay,
        minLength: Drupal.settings.virtual_field_collection.config.autocomplete.minchars,
        position: {
          my: 'right top',
          at: 'right bottom',
          collision: 'none'
        },
        source: function (request, add) {
          // The input element.
          $input = this.element;

          // Determine the data to send to the server.
          var dataToSend = {
            callback: $input.attr('data-callback'),
            filter: $input.attr('data-filter'),
            realm: $input.attr('data-realm')
          };

          // The array for the autocomplete suggestions.
          var suggestions = [];

          // Pass request to server
          $.ajax({
            url: Drupal.settings.virtual_field_collection.config.autocomplete.path + request.term,
            async: true,
            cache: false,
            type: 'POST',
            dataType: 'json',
            data: dataToSend,
            success: function(response) {
              if (response.success) {
                // Add the response payload.
                suggestions = response.data;
              }
              else {
                suggestions.push({value: '--disabled--', label: response.message});
              }

              // Add the suggestions.
              add(suggestions);
            },
            error: function(e) {
              console.log(Drupal.t('AUTOCOMPLETE ERROR') + ': ' + e.message);
              alert(Drupal.t('Oops, something went wrong.'));
            }
          });
        },

        // The "search" event is triggered right before the autocomplete
        // request is sent to the server. We'll add the throbber to the
        // input field here to indicate an active request.
        search: function(event, ui) {
          var $input = $(event.target);
          $input.addClass('throbbing');
          $input.removeAttr('data-value');
        },

        // The "open" event is fired upon presenting the autocomplete
        // suggestions to the user. In this event we'll remove the
        // added throbber class to indicate that the autocomplete
        // request ist completed.
        open: function(event, ui) {
          var $input = $(event.target);
          $input.removeClass('throbbing');
        },

        // The "select" event is fired when the user selects an
        // autocomplete suggestion (not only by clicking on it,
        // $.autocomplete offers also keyboard interaction). We'll
        // separate the data to track: the "label" attribute is
        // displayed inside the text field, while the "real thing"
        // is added as html attribite "data-value" to the form element.
        // Also we have to do a "preventDefault" in here to prevent
        // jQuery from setting it's own value to the form element.
        select: function(event, ui) {
          var $input = $(event.target);
          if (ui.item.value !== '--disabled--') {
            $input.attr({'data-value': ui.item.value}).val(ui.item.label);
          }
          else {
            $input.removeAttr('data-value').val('');
          }
          event.preventDefault();
        }
      });
    },

    /**
     * Unbind all elements with eventhandlers attached from their respective handler.
     * This is done to avoid multiple calls upon re-initialization of form elements.
     */
    unbindEvents: function($root) {
      // Remove all buttons from the event dispatcher.
      $('button.vfcButton', $root).unbind('click');

      // Remove the sorting ability.
      $('.vfcContainer', $root).sortable('destroy');

      // Remove autocomplete functionality.
      $('.vfcAutocomplete').unbind('click').autocomplete('destroy');
    },

    /**
     * Button click event dispatcher.
     * Catches button clicks and calls the appropriate callback function.
     */
    buttonClick: function($button, event) {
      // Determine action to take.
      var action = $button.attr('data-action');

      // Determine the instance the button is located in.
      var $instance = $($button.parents().filter('.vfcAdminArea').get(0));

      // Remove click event handlers in this instance.
      Drupal.vfc.unbindEvents($instance);

      // Try to execute the callback.
      try {
        Drupal.vfc[action].apply($button);
      } catch (err) {
        if (Drupal.vfc[action] === undefined) {
          // There is no callback function defined for this action.
          console.log(Drupal.t('No event handler for action !action', {'!action': action}));
        }
        else {
          // Something went wrong, send the error message to the console log.
          console.log(err.message);
        }
      }

      // Re-attach click event handlers.
      Drupal.vfc.bindEvents($instance);
    },

    /**
     * Button Click Callback: adds an entry to a group.
     */
    addGroupEntry: function($group, data) {
      // The arguments $group and data are only given during the initial interface
      // setup. If this argument is missing, we have to deal with a "real"
      // button click and have to determine the group the button belongs to.
      if ($group === undefined) {
        var $button = $(this);
        $group = $($button.parents().filter('.vfcGroup').get(0));
        data = {
          t: 'GE',
          v: [],
          l: ''
        };
      }

      // Determine the configuration element ID the button relates to.
      var configID = $group.attr('data-configid');

      // Determine the instance name of the virtual field collection the
      // button is located in.
      var instanceName = $group.parents().filter('.vfcAdminArea').attr('data-instancename');

      // Retrieve the configuration settings for this group.
      var config = Drupal.vfc.retrieveElementConfiguration(instanceName, configID);

      // If this group has set a maximum number of allowed entries, check if
      // adding a new entry will exceed that limit.
      if (config.maxEntries !== undefined && Number(config.maxEntries) !== 0) {
        // There is a maximum number of allowed group entries, so count the
        // existing ones in order to prevend exeeding the limit set.
        var entryCount = $('> .vfcContainer > .vfcGroupEntry', $group).length;

        if (entryCount >= Number(config.maxEntries)) {
          // This group is not allowed to take any more entries - throw an error.
          alert(Drupal.t(
            'This group has a limit of maximum entries set and cannot take any more entries!'
          ));

          // Abort adding a new entry to this group.
          return false;
        }
      }

      // All fine, so get a themed entry wrapper.
      var entryWrapper = Drupal.theme(
        'vfcGroupEntryWrapper',
        config,
        data.l
      );

      // Append the entry to the group's DOM tree.
      var $entryWrapper = $(entryWrapper).appendTo($('> .vfcGroupEntries', $group));

      // initialize sub-entries on this entry container
      var $container = $('> .vfcContainer', $entryWrapper);
      Drupal.vfc.initConfiguration(config.fields, [], $container);
    },

    /**
     * Button Click Callback: deletes an entry from a group.
     */
    deleteGroupEntry: function() {
      // Shortcut to the clicked button for better readability.
      var $button = $(this);

      // Determine the group entry the button belongs to.
      var $group = $($button.parents().filter('.vfcGroup').get(0));

      // Ask for the user's permission to delete the group entry.
      if (confirm(Drupal.t('Are you sure you want to delete this group entry?'))) {
        // Permission was given, go on deleting the entry.

        // Determine the vfc field instance the group belongs to.
        var instanceName = $($group.parents().filter('.vfcAdminArea').get(0)).attr('data-instancename');

        // Determine the configuration ID of the group involved.
        var configID = $group.attr('data-configid');

        // Retrieve the DOM element representing the entry.
        var $groupElement = $($button.parents().filter('.vfcGroupEntry').get(0));

        // Remove the entry.
        $groupElement.remove();

        // Count the remaining entries to this group.
        var entryCount = $('> .vfcContainer > .vfcGroupEntry', $group).length;

        // Determine if the group has a minimum number of required entries.
        var groupConfig = Drupal.vfc.retrieveElementConfiguration(instanceName, configID);
        var minEntries = Number(groupConfig.minEntries);

        // If no minimum entries are required by this group, provide at least
        // a single empty group entry.
        if (minEntries === 0) {
          minEntries = 1;
        }

        // If the remaining entries fall below the minimum number of entries to show,
        // add a new empty group entry.
        if (entryCount < minEntries) {
          Drupal.vfc.addGroupEntry($group, {t: 'GE', v: [], l: ''});
        }
      }
    },

    /**
     * Button Click Callback: accordion function.
     */
    showHideContent: function() {
      // Shortcut to the clicked button for better readability.
      var $button = $(this);

      // Determine the element to slide - it's the subsequent DOM element
      // to the clicked button's parent node.
      var $toSlide = $button.parent().next();

      // Determine the state of the element about to slide.
      var state = $toSlide.attr('data-state');

      // Distinct the behaviour depending on the state.
      switch (state) {

        // The element is closed.
        case 'closed':
          // First, set the container state to 'opened' because
          // subsequent function calls may check the state prior
          // to the end of the sliding animation created by jQuery.
          $toSlide.attr({'data-state': 'opened'});

          // Open the element.
          $toSlide.slideDown();

          // Set the button label to the "opened" state.
          $button.text(String.fromCharCode(9650));
          break;

        // The element is opened.
        case 'opened':
        // 'default' will be the case on elements with no prior sliding animation.
        default:
          // First, set the container state to 'closed' because
          // subsequent function calls may check the state prior
          // to the end of the sliding animation created by jQuery.
          $toSlide.attr({'data-state': 'closed'});

          // Close the element.
          $toSlide.slideUp();

          // Set the button label to the "closed" state.
          $button.text(String.fromCharCode(9660));
          break;
      }
    },

    /**
     * Helper function: retrieve the configuration to a given element id.
     *
     * @param instanceName
     * @param configID
     * @param configArray
     * @returns {Object}
     */
    retrieveElementConfiguration: function(instanceName, configID, configArray) {
      // If no configuration array was given as an argument, fetch the one
      // defined for this virtual field collection instance.
      if (configArray === undefined) {
        configArray = Drupal.settings.virtual_field_collection.fields[instanceName].config;
      }

      // Iterate on each single entry made to the configuration.
      for (var varname in configArray) {

        // If the currently examined entry is the one searched, return it.
        if (configArray[varname].configID == configID) {
          return configArray[varname];
        }
        // If the entry is a group, descend into it's fields recursively.
        else if (configArray[varname].type == 'group') {
          var config = Drupal.vfc.retrieveElementConfiguration(instanceName, configID, configArray[varname].fields);
          // If the recursive call found the entry searched, return it.
          if (config) {
            return config;
          }
        }
      }

      // There was no entry matching the configuration ID found.
      return false;
    },

    /**
     * Centralized sub-module API callback: escape html entities.
     */
    htmlEscapeString: function(string) {
      if (string !== undefined && typeof string === 'string' && string.length) {
        // Escape all html entities.
        string = string.replace(/[<>"&]/g, function(match) {
          var replacement = '';
          switch (match) {
            case '<':
              replacement = '&lt;';
              break;

            case '>':
              replacement = '&gt;';
              break;

            case '"':
              replacement = '&quot;';
              break;

            case '&':
              replacement = '&amp;';
              break;
          }
          return replacement;
        });
        return string;
      }
      else {
        // wrong format or undefined - return unchanged.
        return string;
      }
    },

    /**
     * Centralized sub-module API callback: attach autocomplete functionality.
     */
    attachAutoComplete: function($input, callback, filter, realm) {
      // Determine the html tag and tag-type of the input element
      // to attach the autocomplete behaviour to.
      var tag = $input.get(0).nodeName.toLowerCase();
      var type = $input.attr('type').toLowerCase();

      // Autocomplete only works with "input" tags of type "text".
      if (tag === 'input' && type === 'text') {
        $input.attr({
          'data-callback': callback,
          'data-filter': filter,
          'data-realm': realm,
          'autocomplete': 'off'
        }).addClass('vfcAutocomplete');
      }
      else {
        // Alert the user that this input can't be turned into an
        // autocomplete field.
        alert(Drupal.t('Only textfields can be turned into autocomplete fields. Please check your vfc configuration!'));
      }
    },

    /**
     * Helper function: removes empty vfcElements prior to validating
     * the form entries in order to ignore empty fields and groups.
     */
    removeEmptyElements: function(instanceName, $root) {
      // Step 1: remove empty fields
      $('.vfcField', $root).each(function(index, elem) {
        var $field = $(elem);
        var type = $field.attr('data-type');
        var configID = $field.attr('data-configid');
        var $input = $('> .vfcElementContent > .formElement > *', $field);
        var config = Drupal.vfc.retrieveElementConfiguration(
          instanceName,
          configID,
          Drupal.settings.virtual_field_collection.fields[instanceName].config
        );
        try {
          var fieldIsEmpty = Drupal.vfc.fieldapi[type].checkFieldEmpty($input, config);
        } catch (e) {
          var fieldIsEmpty = false;
          console.log(Drupal.t(
            'FATAL ERROR: sub-module for field type "!fieldtype" does not implement mandatory callback function "checkFieldEmpty"!',
            {'!fieldtype': type}
          ));
        }
        if (fieldIsEmpty) {
          $field.remove();
        }
      });

      // the next steps have to be repeated depending on the group/field depth
      for (var i = 0; i < Drupal.settings.virtual_field_collection.fields[instanceName].depth; i++) {
        // Step 2: remove all empty group entries
        $('.vfcGroupEntry', $root).filter(function(index, elem) {
          return $('> .vfcGroupEntryContent > .vfcElement', $(elem)).length === 0;
        }).remove();

        // Step 3: remove all empty groups
        $('.vfcGroup', $root).filter(function(index, elem) {
          return $('> .vfcGroupEntries > .vfcGroupEntry', $(elem)).length === 0;
        }).remove();
      }
    },

    /**
     * JS form submission handler.
     */
    saveFieldValues: function(e) {
      // Determine the form actually submitted.
      var $form = $(e.target);

      // "Global" flag if all validation runs did succeed. We will set this
      // to false if any run fails. It is just used to only alert once.
      var globalSuccess = true;

      // The following steps have to be applied to all instances
      // contained in the form about to be submitted.
      var instances = $form.data('vfc_instances');
      for (var index in instances) {
        // shortcut to the instance
        var $instance = instances[index];

        // Remove potential previous error messages.
        $instance.data('$message_area').empty().hide();

        // Remove all click handlers.
        Drupal.vfc.unbindEvents($instance.data('$root_container'));

        // Remove all possible previous error markings.
        $('*', $instance.data('$root_container')).removeClass('vfcError');

        // Store the current state of the vfc interface.
        var formState = $instance.data('$root_container').clone();

        // Remove all empty elements and groups from the interface
        // prior to the validation run.
        Drupal.vfc.removeEmptyElements($instance.data('name'), $instance.data('$root_container'));

        // Run the validation on the remaining elements.
        var validationResult = Drupal.vfc.validateForm(
          formState,
          $instance.data('$root_container'),
          Drupal.settings.virtual_field_collection.fields[$instance.data('name')].config
        );

        // Restore the form state as it was before the validation run.
        $instance.data('$root_container').replaceWith(formState);

        // Due to the reset we need to "refresh" the $root_container variable
        // to be prepared for the next validation run.
        $instance.data(
          '$root_container',
          $('.vfcRoot', $instance.data('$config_area'))
        );

        // Restore the click handlers.
        Drupal.vfc.bindEvents($instance.data('$root_container'));

        if (validationResult.success) {
          // All fine - serialize the configuration array.
          var values = JSON.stringify(validationResult.values);

          // Write the config to Drupal's FAPI field.
          $instance.val(values);

          // Handle any possible entries to the afterSubmit-Queue.
          if (Drupal.vfc_helper.getObjectLength(Drupal.vfc.queued_operations)) {
            $.ajax({
              url: Drupal.settings.virtual_field_collection.config.rpc,
              async: false,
              cache: false,
              type: 'post',
              data: { operations: Drupal.vfc.queued_operations }
            });
          }
        }
        else {
          // Sort the error messages.
          validationResult.errors.sort();

          // Only display unique error messages.
          var errorDisplay = [];
          var lastError = false;
          for (var index in validationResult.errors) {
            if (validationResult.errors[index] !== lastError) {
              errorDisplay.push(validationResult.errors[index]);
              lastError = validationResult.errors[index];
            }
          }

          // Configuration validation failed - set error messages.
          $instance.data('$message_area').html('<p>' + errorDisplay.join('</p><p>') + '</p>').show();

          // Set the global result flag to false;
          globalSuccess = false;

          // Prevent the form submission.
          e.preventDefault();
        }
      }

      if (!globalSuccess) {
        // Alert the user if any validation run failed.
        alert(Drupal.t('Please check your form inputs.'));
      }
    },

    /**
     * Form validation.
     *
     * This function iterates recursively through all vfcElements defined in
     * the virtual field collection configuration for this field instance.
     * We will mark all errors on the "real" interface as given in the
     * "formState" argument, not on the DOM objects we actually examine. This
     * is necessary because the actual DOM in cleared of any empty fields and
     * groups and will be replaced with our "live copy" on failed validation.
     *
     * @param {Object} formState
     *                 the "live copy" of the interface before the validation call
     * @param {Object} $root (optional)
     *                 The root element of the actual DOM we examine (vfcRoot is default)
     * @param {Object} config (optional)
     *                 The configuration section we actually examine (root config is default)
     */
    validateForm: function(formState, $root, config) {
      // Prepare the result object for the current iteration.
      var result = {
        // We estimate a good outcome - if the validation run stumbles
        // upon any error, this flag will be set to false.
        success: true,
        errors: [],
        values: {}
      };

      // Iterate on each single configuration entry.
      for (var varname in config) {

        // Shortcut to the configuration settings for the current element.
        var configEntry = config[varname];

        // Distinct the validation behaviour based on the entry type.
        switch (configEntry.type) {

          // The actual examined configuration element is a group.
          case 'group':
            // Try to fetch the DOM element for this configuration entry.
            var $group = $('div.vfcGroup[data-configid="' + configEntry.configID + '"]', $root);

            // We have found a DOM element representing that group.
            if ($group.length > 0) {

              // Prepare the group data array.
              // Note: the inner array will hold the group entry labels.
              var groupData = [[]];

              // Determine the entries made to this group.
              var $entries = $('> .vfcGroupEntries > .vfcGroupEntry', $group);

              // Check if the minimum number of entries was not met.
              if (Number(configEntry.minEntries) > 0 && $entries.length < Number(configEntry.minEntries)) {

                // Mark the group as faulty.
                $('div.vfcGroup[data-configid="' + configEntry.configID + '"]', formState).addClass('vfcError');

                // Set an appropriate error message.
                result.errors.push(
                  Drupal.t(
                    'The group "!grouplabel" needs at least !minEntries entries, but only !entries entries were made.',
                    {
                      '!grouplabel': configEntry.groupLabel,
                      '!minEntries': Number(configEntry.minEntries),
                      '!entries': $entries.length
                    }
                  )
                );

                // Mark this validation run as failed.
                result.success = false;
              }

              // Next, iterate on each entry made to the group and validate it.
              $entries.each(function(delta, elem) {

                // Shortcut to the current group entry.
                var $entry = $(elem);

                // Shortcut to the group entry content.
                var $entryContent = $('> .vfcGroupEntryContent', $entry);

                // Recurse on the group entry's content for validation.
                var subValidation = Drupal.vfc.validateForm(formState, $entryContent, configEntry.fields);

                // Group labels are always the FIRST element contained in a group
                // in order to save overhead data on the value object.
                // If the group provides a group label field, retrieve the group
                // label and add it's value to the group entry data object.
                if (configEntry.groupLabelField) {
                  groupData[0].push($('> .vfcElementHeader > label input[data-name="groupLabel"]', $entry).val());
                }
                // The group provides no override label field, so set the default
                // group label value for the label.
                else {
                  groupData[0].push(configEntry.groupLabel);
                }

                // Add the processed entry data object to the group's value array.
                //groupData.push(entryData);
                groupData.push(subValidation.values);

                // If there were errors detected on the entry content, track them.
                if (!subValidation.success) {

                  // Make sure to mark this group entry as faulty.
                  var instanceID = $entry.attr('data-instanceid');
                  $('div.vfcElement[data-instanceid="' + instanceID + '"]', formState).addClass('vfcError');

                  // Flag this validation run as failed.
                  result.success = false;

                  // Keep track of all error messages.
                  result.errors = result.errors.concat(subValidation.errors);
                }
              });

              // Add the processed group data object to the result's value array.
              result.values[varname] = groupData;
            }
            // We did not find any DOM elements representing the configured group
            // because it was empty and therefore removed prior to the validation run.
            // Determine if this group has a minimum number of entries set in it's
            // configuration settings. If this is the case, the missing DOM elements
            // indicate an error.
            else if (Number(configEntry.minEntries) > 0) {
              // Mark the group as faulty.
              $('div.vfcGroup[data-configid="' + configEntry.configID + '"]', formState).addClass('vfcError');

              // Set an appropriate error message.
              result.errors.push(
                Drupal.t(
                  'The group "!grouplabel" needs at least !minEntries entries.',
                  {
                    '!grouplabel': configEntry.groupLabel,
                    '!minEntries': Number(configEntry.minEntries)
                  }
                )
              );

              // Mark this validation run as failed.
              result.success = false;
            }
            break;

          // The actual examined configuration element is a field.
          default:
            // Try to fetch the DOM element for this configuration entry.
            var $field = $('div.vfcField[data-configid="' + configEntry.configID + '"]', $root);

            // Determine the field's value.
            if ($field.length) {
              // A shortcut to the field's input element(s).
              var $input = $('> .vfcElementContent > .formElement > *', $field);

              // The instance id of this field.
              var instanceID = $field.attr('data-instanceid');

              // Shortcut to the appropriate field api object.
              var fieldapi = Drupal.vfc.fieldapi[configEntry.type];

              // Let the sub-module validate it's value.
              try {
                var fieldIsValid = fieldapi.validateField($input, configEntry);
              } catch (e) {
                // The sub-module either fails to implement the mandatory
                // callback "validateField" or it's execution failed. Due to
                // this we cannot assume this field is valid. Moreover, we have
                // to alert the user of the faulty sub-module in order to enable
                // him/her to deactivate it.
                var fieldIsValid = false;
                alert(Drupal.t(
                  'The sub-module providing the field type "!fieldtype" failed on implementing the mandatory callback "validateField".',
                  {'!fieldtype': configEntry.type}
                ));
                console.log(e.message);
              }

              // If the sub-module returned anything other than "true"
              // this is assumed an error.
              if (fieldIsValid !== true) {

                // Flag the validation run as failed.
                result.success = false;

                // Mark the field as faulty.
                $('div.vfcField[data-instanceid="' + instanceID + '"]', formState).addClass('vfcError');

                // Determine if the sub-module returned an error message.
                if (fieldIsValid !== false) {
                  // Generally spoken, the vfc api expects an array containing error messages.
                  // But we will be nice to our developers and accept plain strings for convenience
                  // reasons. :-)
                  if (fieldIsValid instanceof Array) {
                    result.errors = result.errors.concat(fieldIsValid);
                  }
                  else if (typeof fieldIsValid === 'string') {
                    result.errors.push(fieldIsValid);
                  }
                  else {
                    // The returned value is neither of type boolean nor a string or an array.
                    // As a last fallback, we will set an error message on our own.
                    result.errors.push(Drupal.t(
                      'Please check your input on "!fieldlabel".',
                      {'!fieldlabel': configEntry.label}
                    ));
                  }
                }
              }
              else {
                // This field passed it's validation process. This is the place where
                // the prepareSerialization callback will get fired.
                if (fieldapi.prepareSerialization !== undefined) {
                  try {
                    fieldapi.prepareSerialization($input, configEntry);
                  } catch (e) {
                    console.log(Drupal.t(
                      'Sub-Module providing field type "!fieldtype" failed executing it\'s prepareSerialization callback.',
                      {'!fieldtype': configEntry.type}
                    ));
                  }
                }
              }

              try {
                var value = fieldapi.getFieldValue($input, configEntry);
              } catch (e) {
                // The sub-module either fails to implement the mandatory
                // callback "getFieldValue" or it's execution failed. Due to
                // this we cannot retrieve the field's value. Moreover, we have
                // to alert the user of the faulty sub-module in order to enable
                // him/her to deactivate it.
                var value = null;

                // Flag the validation run as failed.
                result.success = false;

                // Alert the user with an appropriate error message.
                if (fieldapi.getFieldValue !== undefined) {
                  alert(Drupal.t(
                    'The getFieldValue callback of field type "!fieldtype" failed execution. Error message: !message',
                    {
                      '!fieldtype': configEntry.type,
                      '!message': e.message
                    }
                  ));
                }
                else {
                  alert(Drupal.t(
                    'The sub-module providing the field type "!fieldtype" does not provide the mandatory callback "getFieldValue".',
                    {'!fieldtype': configEntry.type}
                  ));
                }
              }
            }
            else {
              // The field was empty (and therefore it's DOM element was removed
              // during the preparation of this validation run). It will be nevertheless
              // tracked within the result data with a null value.
              var value = null;

              // If this field was set to required in the configuration, this
              // is also an error we have to track.
              if (configEntry.required) {

                // Mark the validation run as failed.
                result.success = false;

                // Set an appropriate error message.
                result.errors.push(Drupal.t(
                  'The field "!fieldlabel" is a required field.',
                  {'!fieldlabel': configEntry.label}
                ));

                // Determine the parameters needed for error marking.
                var parentInstanceID = $root.attr('data-instanceid');
                var fieldConfigID = configEntry.configID;

                // Determine the field wrapper element.
                var $instanceWrapper = $('div[data-instanceid="' + parentInstanceID + '"]', formState);

                // If no field wrapper was found, it was the root container itself (we only
                // searched inside the root container).
                if ($instanceWrapper.length === 0) {
                  $instanceWrapper = formState;
                }

                // Determine the field instance.
                var $fieldInstance = $('div[data-configid="' + fieldConfigID + '"]', $instanceWrapper);

                // Mark the field as faulty.
                $fieldInstance.addClass('vfcError');
              }
            }

            // Push the field data object to the result value array.
            result.values[varname] = value;
            break;
        }

      };

      return result;
    },

    /**
     * Adds a sub-module operation to the operation queue.
     *
     * @param {object} operation
     *  object structure:
     *  {
     *    module: module_name
     *    hook: hook_name
     *    args: [...] (Array)
     *  }
     * @return {int} queue id
     */
    processAfterSubmit: function(operation) {
      Drupal.vfc.queued_operations[++Drupal.vfc.queued_operations.queueID] = operation;
      return Drupal.vfc.queued_operations.queueID;
    },

    /**
     * Removes a queued operation from the operation queue.
     */
    removeFromOperationQueue: function(opId) {
      if (Drupal.vfc.queued_operations[opId] !== undefined) {
        delete(Drupal.vfc.queued_operations[opId]);
        return true;
      } else {
        return false;
      }
    },

    /**
     * object placeholder for submodule api implementations
     */
    fieldapi: {},

    /**
     * a collection of theme hooks used by this interface
     */
    theme: {
      /**
       * Themes an interface button.
       *
       * @param {string} callback
       *        function callback for the button
       * @param {string} label
       *        text for the button label
       * @param {array} attributes (optional)
       *        additional html attributes for the button tag
       * @returns {string}
       */
      vfcButton: function(callback, label) {
        return '<button class="vfcButton" data-action="' + callback + '">' + label + '</button>';
      },

      /**
       * Themes a group container.
       */
      vfcGroup: function(config) {
        var instanceID = Drupal.vfc_helper.generateUniqueID();
        var vfcGroup = '<div class="vfcElement vfcGroup" data-type="vfcGroup" data-configid="' + config.configID + '" data-instanceid="' + instanceID + '">' +
                         '<div class="vfcContainer vfcGroupEntries"></div><div class="vfcElementFooter">' +
                           Drupal.theme('vfcButton', 'addGroupEntry', config.buttonLabel) +
                       '</div></div>';
        return vfcGroup;
      },

      /**
       * Themes a group entry wrapper.
       */
      vfcGroupEntryWrapper: function(config, customGroupLabel) {
        if (config.groupLabelField) {
          if (customGroupLabel === undefined) {
            customGroupLabel = '';
          }
          var label = '<label>' + config.groupLabel + ': ' +
            '<input type="text" class="form-text" data-name="groupLabel" value="' + customGroupLabel + '"/></label>';
        }
        else {
          var label = '<label>' + config.groupLabel + '</label>';
        }
        var instanceID = Drupal.vfc_helper.generateUniqueID();
        var entry = '<div class="vfcElement vfcGroupEntry" data-type="vfcGroupEntry" data-instanceid="' + instanceID + '">' +
                    '<div class="vfcElementHeader vfcGroupEntryHeader">' +
                      Drupal.theme('vfcButton', 'deleteGroupEntry', '&times;') +
                      Drupal.theme('vfcButton', 'showHideContent', '&#9650;') +
                      label +
                    '</div><div class="vfcContainer vfcGroupEntryContent"></div></div>';
        return entry;
      },

      /**
       * Themes a field element.
       */
      vfcField: function(varname, config, data) {
        try {
          var formElement = Drupal.vfc.fieldapi[config.type].getFormElement(varname, config, data);
        } catch (e) {
          if (Drupal.vfc.fieldapi[config.type].getFormElement === undefined) {
            var formElement = Drupal.t(
              'FATAL ERROR: mandatory sub-module callback "getFormElement" missing for field type !fieldtype!',
              {'!fieldtype': config.type}
            );
          }
          else {
            var formElement = Drupal.t(
              'FATAL ERROR on mandatory callback "getFormElement" for field type !fieldtype: ',
              {'!fieldtype': config.type}
            ) + e.message;

          }
        }
        var instanceID = Drupal.vfc_helper.generateUniqueID(),
            description = config.description.length ? '<div class="fielddescription"><p>' + config.description + '</p></div>' : '';
        return '<div class="vfcElement vfcField" data-type="' + config.type + '" data-configid="' + config.configID + '" data-instanceid="' + instanceID + '">' +
                 '<div class="vfcElementHeader"><label>' +
                    config.label + (config.required ? Drupal.vfc.form_required_markup : '') +
                 '</label></div><div class="vfcElementContent"><div class="formElement">' +
                     formElement +
                   '</div>' + description + '</div>' +
               '</div>';
      }
    }
  };

  /**
   * Startup functions
   */
  Drupal.behaviors.virtual_field_collection = {
    attach: function (context, settings) {
      // Extend drupal with our own js theme hooks.
      $.extend(Drupal.theme.prototype, Drupal.vfc.theme);

      // initialize the interface
      $('textarea.vfc_serialized_data').once(function(index, field){
        Drupal.vfc.initInterface(field);
      });
    }
  }
}(jQuery));
