/**
 * @file
 * Contains the javascript implementation for the virtual field collection
 * field formatter / display settings configuration interface.
 */

/**
 * Function library for the field formatter settings interface.
 */
(function ($) {
  Drupal.vfc = {

    // Preparation of the default display settings.
    commonDisplaySettings: {
      elementDisplay: {
        label: Drupal.t('Element display'),
        type: 'select',
        default: 'show',
        options: {
          show: Drupal.t('Shown'),
          hide: Drupal.t('Hidden')
        }
      },
      wrapperElement: {
        label: Drupal.t('Wrapper element'),
        type: 'select',
        default: Drupal.settings.virtual_field_collection.display_defaults.wrapperElement,
        options: {
          none: Drupal.t('None'),
          div: Drupal.t('HTML DIV Container'),
          p: Drupal.t('HTML Paragraph'),
          span: Drupal.t('HTML Span')
        }
      },
      wrapperCssClass: {
        label: Drupal.t('Wrapper CSS class'),
        type: 'text',
        default: Drupal.settings.virtual_field_collection.display_defaults.wrapperCssClass
      },
      label: {
        label: Drupal.t('Label display'),
        type: 'select',
        default: Drupal.settings.virtual_field_collection.display_defaults.label,
        options: {
          shown: Drupal.t('Shown'),
          hidden: Drupal.t('Hidden')
        }
      },
      labelWrapper: {
        label: Drupal.t('Label wrapper'),
        type: 'select',
        default: Drupal.settings.virtual_field_collection.display_defaults.labelWrapper,
        options: {
          none: Drupal.t('None'),
          p: Drupal.t('HTML Paragraph'),
          span: Drupal.t('HTML Span')
        }
      }
    },

    // Preparation of some local variables.
    element_hidden_label_markup: ' <span class="elementHidden">(' + Drupal.t('Hidden') + ')</span>',
    element_settings_path: '> .vfcElementContentWrapper > .vfcElementSettingsWrapper > .vfcElementSettings > table > tbody > tr',

    /**
     * Cache some dom elements to speed things up.
     *
     * This function is called by initInterface, which itself is fired upon $(document).ready
     */
    cacheDOMElements: function($instance) {
      var $form = $($('textarea.vfc_serialized_data').parents().filter('form').get(0));
      var $config_area = $('#vfcAdminArea', $form);
      $instance.data({
        '$form': $form,
        '$submit_button': $('input[type="submit"][value="' + Drupal.t('Update') + '"]', $form),
        '$config_field': $('textarea.vfc_serialized_data', $form),
        '$config_area': $config_area,
        '$root_container': $('> .vfcElement > .vfcRoot', $config_area)
      });
    },

    /**
     * Initially called to build up the interface.
     */
    initInterface: function(field) {
      // shortcut to the instance
      var $instance = $(field);

      // One-time shot to cache the VFC DOM elements.
      Drupal.vfc.cacheDOMElements($instance);

      // Hide the wrapper element of the serialized configuration data field.
      $instance.data('$config_area').prev().hide();

      // Bind the vfc submit handler to our own form submission handler.
      Drupal.ajax[$instance.data('$submit_button').attr('id')].beforeSerialize = function(element, options) {
        Drupal.vfc.saveConfiguration($instance);
        // Call the parent ("original") form submission handler.
        Drupal.ajax.prototype.beforeSerialize(element, options);
      }

      // Now we can enable the previously disabled submit button.
      // @see virtual_field_collection.module:
      // virtual_field_collection_field_formatter_settings_form_afterbuild()
      $instance.data('$submit_button').removeAttr('disabled');

      // Hide the "noJS" warning.
      $('.noJS', $instance.data('$config_area')).remove();

      // Initialize already existing groups and fields (if any).
      if (Drupal.vfc_helper.getObjectLength(Drupal.settings.virtual_field_collection.config)) {
        Drupal.vfc.initConfiguration(
          Drupal.settings.virtual_field_collection.config,
          Drupal.settings.virtual_field_collection.display,
          Drupal.settings.virtual_field_collection.available_view_modes,
          $instance.data('$root_container')
        );
      }

      // Initialize click event handlers.
      Drupal.vfc.bindEvents($instance);
    },

    /**
     * Initializes an existing field configuration.
     *
     * @param {Object} config
     * @param {Object} display
     * @param {Object} availableViewModes
     * @param {HTMLElement} $root
     */
    initConfiguration: function(config, display, availableViewModes, $root) {
      // Process each single entry in the config array.
      for (var varname in config) {
        // Only iterate on own properties.
        if (!config.hasOwnProperty(varname)) {
          continue;
        }

        // Shortcut to the element's configuration.
        var element = config[varname];

        // Try to determine if there is a display setting
        // for this element.
        if (display[varname] !== undefined) {
          var displayfields = display[varname].fields;
          var displaySettings = display[varname].display;
          var delta = display[varname].delta;
        }
        else {
          var displayfields = [];
          var displaySettings = {};
          var delta = -1;
        }

        // Determine the behaviour based on the current element's type.
        switch (element.type) {
          case 'group':
            // The currently processed element is a group - add the group.
            var $group = Drupal.vfc.addGroup(
              $root,
              varname,
              element,
              displaySettings,
              delta
            );

            // Descend recursively through the group's content.
            Drupal.vfc.initConfiguration(
              element.fields,
              displayfields,
              availableViewModes[varname],
              $group
            );
            break;

          default:
            // The actual element is just a plain field.
            Drupal.vfc.addField(
              $root,
              varname,
              element,
              displaySettings,
              delta,
              availableViewModes[varname]
            );
            break;

        }
      }
    },

    /**
     * Add a group to the form interface.
     *
     * @param {HTMLElement} $container
     * @param {string} varname
     * @param {Object} config
     * @param {Object} displaySettings
     * @param {int} delta
     * @returns {HTMLElement}
     */
    addGroup: function($container, varname, config, displaySettings, delta) {
      // Get a themed group element with all possible arguments.
      var group = Drupal.theme(
        'vfcGroup',
        varname,
        config,
        displaySettings
      );

      // Append the new group to the active container.
      // TODO
      // - respect configured weight/delta
      if (delta !== -1) {
        var $group = $(group).appendTo($container);
      }
      else {
        var $group = $(group).appendTo($container);
      }

      // Return the new group element.
      return $('> .vfcElementContentWrapper > .vfcGroupContentWrapper > .vfcContainer', $group);
    },

    /**
     * Add a field to the form interface.
     *
     * @param {HTMLElement} $container
     * @param {string} varname
     * @param {Object} config
     * @param {object} displaySettings
     * @param {int} delta
     * @param {object} availableViewModes
     */
    addField: function($container, varname, config, displaySettings, delta, availableViewModes) {
      // Get a themed field element prepared for DOM insertion.
      var field = Drupal.theme(
        'vfcField',
        varname,
        config,
        displaySettings,
        availableViewModes
      );

      // Append the new field element to the target container.
      var $field = $(field).appendTo($container);

      // If there was a delta value given, sort the container.
      if (delta !== -1) {
        // Assign the configured delta value to the element.
        $field.attr({'data-delta': delta});
      }
    },

    /**
     * Bind all clickable elements to their respective click handler.
     */
    bindEvents: function($instance) {
      // Bind all buttons to the button click event dispatcher.
      $('button.vfcButton', $instance.data('$config_area'))
        .unbind('click')
        .bind('click', function(e) {
          e.preventDefault();
          e.stopPropagation();
          Drupal.vfc.buttonClick($(this), e);
        })
        .data('instance', $instance);

      // Initialize the sorting ability.
      $('.vfcContainer', $instance.data('$config_area')).sortable({
        handle: '.vfcElementHeader',
        containment: 'parent',
        axis: 'y'
      });

      // Add a change handler to the element display selectors.
      $('select[data-type="elementDisplay"]', $instance.data('$config_area')).change(function() {
        Drupal.vfc.showHideElement.apply($(this));
      });
    },

    /**
     * Unbind all clickable elements from their respective click handler.
     * This is done to avoid multiple calls upon re-initialization of form elements.
     */
    unbindEvents: function($instance) {
      // Remove all buttons from the event dispatcher.
      $('button.vfcButton', $instance.data('$config_area')).unbind('click');

      // Remove the sorting ability.
      $('.vfcContainer', $instance.data('$root_container')).sortable('destroy');

      // Remove the showHideElement change handler.
      $('select[data-type="elementDisplay"]', $instance.data('$config_area')).unbind('change');
    },

    /**
     * Button click event dispatcher.
     *
     * Catches button clicks and tries to call the appropriate callback
     * function. Will throw a "silent error" (console output) if the
     * callback fails.
     *
     * @param {HTMLElement} $button
     * @param {Object} event
     */
    buttonClick: function($button, event) {
      // Determine the action to take.
      var action = $button.attr('data-action');

      // Remove all existing click event handlers prior to
      // executing the callback to prevent interference and/or
      // possible negative side effects.
      Drupal.vfc.unbindEvents($button.data('instance'));

      // Try to execute the callback if it exists.
      try {
        Drupal.vfc[action].apply($button);
      } catch (err) {
        // If the event handler is not defined or fails execution,
        // log a warning message to the console.
        if (Drupal.vfc[action] === undefined) {
          console.log(Drupal.t('Error: No event handler for !action.', {'!action': action}));
        }
        else {
          console.log(Drupal.t('Error') + ': ' + err.message);
        }
      }

      // Re-attach the click event handlers.
      Drupal.vfc.bindEvents($button.data('instance'));
    },

    /**
     * Button Click Callback: accordion function.
     */
    showHideContent: function() {
      // Shortcut to the clicked button for better readability.
      var $button = $(this);

      // Determine the element to slide - it's the subsequent DOM element
      // to the clicked button's parent node.
      var $toSlide = $button.parent().next();

      // Determine the state of the element about to slide.
      var state = $toSlide.attr('data-state');

      // Distinct the behaviour depending on the state.
      switch (state) {

        // The element is closed.
        case 'closed':
          // First, set the container state to 'opened' because
          // subsequent function calls may check the state prior
          // to the end of the sliding animation created by jQuery.
          $toSlide.attr({'data-state': 'opened'});

          // Open the element.
          $toSlide.slideDown();

          // Set the button label to the "opened" state.
          $button.text(String.fromCharCode(9650));
          break;

        // The element is opened.
        case 'opened':
        // 'default' will be the case on elements with no prior sliding animation.
        default:
          // First, set the container state to 'closed' because
          // subsequent function calls may check the state prior
          // to the end of the sliding animation created by jQuery.
          $toSlide.attr({'data-state': 'closed'});

          // Close the element.
          $toSlide.slideUp();

          // Set the button label to the "closed" state.
          $button.text(String.fromCharCode(9660));
          break;

      }
    },

    /**
     * Select Change Callback: show / hide element.
     */
    showHideElement: function() {
      var $select = $(this);
      var $element = $($select.parents().filter('.vfcElement').get(0));
      var $settingsTable = $($select.parents().filter('table').get(0));
      var elementType = $element.attr('data-type');
      var selectedOption = $('option:selected', $select).val();
      switch (selectedOption) {
        case 'hide':
          $('> .vfcElementHeader > label', $element).append(Drupal.vfc.element_hidden_label_markup);
          $('> tbody > tr', $settingsTable).not($select.parent().parent()).hide();
          if (elementType === 'vfcGroup') {
            $('> .vfcElementContentWrapper > .vfcGroupContentWrapper', $element).hide();
          }
          break;

        case 'show':
          $('> .vfcElementHeader > label > span', $element).remove();
          $('> tbody > tr', $settingsTable).not($select.parent().parent()).show();
          if (elementType === 'vfcGroup') {
            $('> .vfcElementContentWrapper > .vfcGroupContentWrapper', $element).show();
          }
          break;

      }
    },

    /**
     * Helper function: prepares the setting object and pre-fills the
     * appropriate values.
     *
     * @param {Object} elementSpecificSettings
     * @param {Object} settingValues
     * @param {Object} config
     * @returns {Object}
     */
    prepareElementSettings: function(elementSpecificSettings, settingValues, config) {
      // Create a new object to hold all settings in order to prevent referencing.
      var settingsObject = {};

      // Clone the common settings object in order to leave it unchanged.
      $.extend(settingsObject, Drupal.vfc.commonDisplaySettings);

      // Add a hint if a group provides a custom label.
      if (config.type === 'group' && config.groupLabelField) {
        settingsObject.label.label += ' (' + Drupal.t('Custom label') + ')';
      }
      else {
        settingsObject.label.label = Drupal.t('Label display');
      }

      // Merge the element specific settings into the common ones.
      $.extend(settingsObject, elementSpecificSettings);

      // Walk each of the settings and check for either actual value overrides or value defaults.
      for (var setting in settingsObject) {
        // Only iterate on real properties, not inherited ones.
        if (!settingsObject.hasOwnProperty(setting)) {
          continue;
        }

        if (settingValues[setting] !== undefined) {
          // There is a actual setting value present, this has priority.
          settingsObject[setting].value = settingValues[setting];
        }
        else if (settingsObject[setting].default != undefined) {
          // There is no setting value present, but a default value exists.
          settingsObject[setting].value = settingsObject[setting].default;
        }
      }

      // Return the readily processed settings object.
      return settingsObject;
    },

    /**
     * JavaScript form submission handler.
     *
     * This is called upon clicking the "save configuration" button. It will
     * call the buildConfiguration function and set the serialized field
     * display configuration to Drupal's "real" FAPI field.
     *
     * @param {Object} e
     */
    saveConfiguration: function($instance) {
      // Call the configuration builder - this will recursively iterate all
      // elements configured and build up the configuration object.
      var configuration = Drupal.vfc.buildConfiguration($instance.data('$root_container'));

      // Store the actual setting in the settings object
      Drupal.settings.virtual_field_collection.config = configuration;

      // Serialize the configuration array.
      var config = JSON.stringify(configuration);

      // Write the config to Drupal's FAPI field.
      $instance.data('$config_field').val(config);

      // Alert the user of saving the form.
      alert(Drupal.t('Changes mage will not be saved until this form is submitted.'));
    },

    /**
     * Builds the virtual field collection display configuration.
     *
     * This function iterates recursively through all configured groups
     * and fields of this virtual field collection.
     *
     * @param {HTMLElement} $root (optional)
     * @param {int} depth (optional)
     * @returns {object}
     */
    buildConfiguration: function($root) {
      // Result object declaration for the current run.
      var config = {};

      // Iterate on each contained element...
      $('> .vfcElement', $root).each(function(delta, elem) {
        // Shortcut to the element.
        var $element = $(elem);

        // Determine the variable name of the current element.
        var varname = $element.attr('data-varname');

        // Prepare the result object.
        config[varname] = { delta: delta };

        // Collect the settings made to this element.
        var display = {};
        $(Drupal.vfc.element_settings_path, $element).each(function(index, entry) {
          var $settingRow = $(entry);
          var $input = $('td:last-child > *', $settingRow);
          var type = $input.get(0).nodeName.toLowerCase();
          var setting = $settingRow.attr('data-setting');
          switch (type) {
            case 'select':
              var value = $('option:selected', $input).val();
              break;

            case 'input':
              var subtype = $input.attr('type');
              switch (subtype) {
                case 'text':
                  var value = $input.val();
                  break;

                case 'checkbox':
                  var value = $input.attr('checked');
                  break;

              }
              break;
          }
          display[setting] = value;
        });

        // Element is a group of fields / groups.
        if ($element.attr('data-type') === 'vfcGroup') {
          config[varname].display = display;
          config[varname].fields = Drupal.vfc.buildConfiguration(
            $('> .vfcElementContentWrapper > .vfcGroupContentWrapper > .vfcContainer', $element)
          )
        }
        // Element is a plain field.
        else {
          config[varname].display = display;
        }
      });

      // Return the configuration.
      return config;
    },

    /**
     * A collection of theme hooks used by this interface.
     */
    theme: {
      /**
       * Themes an interface button.
       *
       * @param {string} callback
       *        function callback for the button
       * @param {string} label
       *        text for the button label
       * @param {array} attributes (optional)
       *        additional html attributes for the button tag
       * @returns {string}
       */
      vfcButton: function(callback, label) {
        return '<button class="vfcButton" data-action="' + callback + '">' + label + '</button>';
      },

      /**
       * Themes a settings widget.
       *
       * @param {object} settings
       *        an object of objects for the element settings
       *        - key: name of the setting
       *        - value: {label, type, [options], default, [value]}
       * @param {boolean} showAccordionButton
       * @returns {string}
       */
      vfcElementSettings: function(settings, showAccordionButton) {
        if (settings === undefined || typeof settings !== 'object') {
          return '';
        }
        else {
          if (showAccordionButton === undefined) {
            showAccordionButton = false;
          }
          // Header of the setting widget.
          var settingsWidget = '<div class="vfcElementSettingsWrapper">' +
            '<div class="vfcElementHeader vfcElementSubHeader">' +
            (showAccordionButton ? Drupal.theme('vfcButton', 'showHideContent', '&#9650;') : '') +
            '<label>&nbsp;&nbsp;' + Drupal.t('Settings') + '</label>' +
            '</div><div class="vfcElementSettings" data-type="elementSettings">' +
            '<table><thead><tr>' +
            '<th class="vfcSettingLabel">' + Drupal.t('Setting') + '</th>' +
            '<th>' + Drupal.t('Value') + '</th>' +
            '</tr></thead><tbody>';

          // The settings one by one.
          for (var setting in settings) {
            // Opening the markup for the current setting.
            var settingRequired = settings[setting].required !== undefined && settings[setting].required;
            settingsWidget += '<tr data-setting="' + setting + '"><td class="vfcSettingLabel">' +
              Drupal.t(settings[setting].label) +
              (settingRequired ? Drupal.vfc.form_required_markup : '') +
              '</td><td>';

            // The setting's value.
            var settingValue = settings[setting].value !== undefined
              ? settings[setting].value
              : (settings[setting].default !== undefined
              ? settings[setting].default
              : (settings[setting].type == 'set' ? [] : false)
              );

            // The input element(s).
            settingsWidget += Drupal.theme(
              'vfcInputElement',
              settings[setting].type,
              setting,
              settingValue,
              (settings[setting].options !== undefined ? settings[setting].options : null)
            );

            // Closing the markup for the current setting.
            settingsWidget += '</td></tr>';
          }

          // Footer of the settings widget.
          settingsWidget += '</tbody></table></div></div>';

          // Return the widget markup.
          return settingsWidget;
        }
      },

      /**
       * Themes a vfc input element.
       *
       * Note:   These form elements intentionally do NOT use a "name" attribute!
       * Reason: Since these are "virtual" fields unknown to the drupal backend there is
       *         no need to post them back to the server. Input elements without a name
       *         attribute are simply ignored by the browser on form submission.
       *
       * @param {string} type
       * @param {string} name
       * @param {*} value
       * @param {Array} options (optional)
       * @returns {string}
       */
      vfcInputElement: function(type, name, value, options) {
        if (options === undefined || typeof options !== 'object') {
          options = {};
        }
        var element;
        switch (type) {
          case 'text':
          case 'int':
            if (value === undefined || value == null) {
              value = false;
            }
            element = '<input type="text" data-type="' + name + '" class="form-text" data-type="' + type + '"';
            if (value) {
              element += ' value="' + (type === 'int' && value.length ? Number(value) : value) + '"';
            }
            element += '/>';
            break;

          case 'bool':
            element = '<input type="checkbox" data-type="' + name + '" value="1"';
            if (value === true) {
              element += ' checked="checked"';
            }
            element += '/>';
            break;

          case 'set':
            element = '';
            for (var option in options) {
              element += '<div><input type="checkbox" data-type="' + name + '" value="' + option + '"';
              if (value.indexOf(option) != -1) {
                element += ' checked="checked"';
              }
              element += '/> ' + options[option] + '</div>';
            }
            break;

          case 'select':
            element = '<select data-type="' + name + '">';
            for (var option in options) {
              element += '<option value="' + option + '"';
              if (value === option) {
                element += ' selected="selected"';
              }
              element += '> ' + options[option] + '</option>';
            }
            element += '</select>';
            break;
        }
        return element;
      },

      /**
       * Themes a vfc element container.
       *
       * @param {string} type
       *        the type of the element
       * @param {string} varname
       *        the variable name of the element
       * @param {string} title
       *        the title for the element header
       * @param {Array} buttons (optional)
       *        buttons to place in the header
       * @param {Array} settings (optional)
       *        array of element settings
       * @param {string} content (optional)
       *        content of the element
       * @returns {string}
       */
      vfcElement: function(type, varname, title, buttons, settings, content) {
        // Default values for optional arguments.
        if (buttons === undefined || typeof buttons !== 'object') {
          buttons = '';
        }
        else {
          buttons = buttons.join('');
        }
        if (settings === undefined || typeof settings !== 'object') {
          settings = {};
        }
        if (content === undefined) {
          content = '';
        }

        // Construct the element.
        var vfcElement = '<div class="vfcElement" data-type="' + type + '" data-varname="' + varname + '">' +
          '<div class="vfcElementHeader">' +
          buttons +
          '<label>&nbsp;&nbsp;' +
          title +
          '</label></div><div class="vfcElementContentWrapper" data-type="elementContent">' +
          Drupal.theme('vfcElementSettings', settings, type == 'vfcGroup') +
          content +
          '</div></div>';
        return vfcElement;
      },

      /**
       * Themes a group container.
       *
       * @param {string} varname
       * @param {Object} config
       * @param {Object} displaySettings
       * @returns {string}
       */
      vfcGroup: function(varname, config, displaySettings) {
        // Prepare group specific settings.
        var groupSettings = {
          groupFormat: {
            label: Drupal.t('Group content format'),
            type: 'select',
            default: Drupal.settings.virtual_field_collection.display_defaults.groupFormat,
            options: {
              none: Drupal.t('None'),
              ul: Drupal.t('Unordered list'),
              ol: Drupal.t('Ordered list'),
              div: Drupal.t('DIV Container')
            }
          },
          entryCssClass: {
            label: Drupal.t('Entry CSS class'),
            type: 'text',
            default: ''
          },
          zebra: {
            label: Drupal.t('Add zebra classes'),
            type: 'bool',
            default: true
          },
          firstlast: {
            label: Drupal.t('Add first / last classes'),
            type: 'bool',
            default: true
          }
        };

        // Get a complete settings object prepared for inclusion.
        var settings = Drupal.vfc.prepareElementSettings(groupSettings, displaySettings, config);

        // Buttons for the element header.
        var buttons = [
          Drupal.theme('vfcButton', 'showHideContent', '&#9650;')
        ];

        // The field container markup as content for the element.
        var fieldContainer = '<div class="vfcGroupContentWrapper">' +
          '<div class="vfcElementHeader vfcElementSubHeader">' +
          Drupal.theme('vfcButton', 'showHideContent', '&#9650;') +
          '<label>&nbsp;&nbsp;' + Drupal.t('Fields') + '</label>' +
          '</div><div class="vfcContainer" data-type="groupContent">' +
          '</div></div>';

        // Theme the element and return it.
        return Drupal.theme(
          'vfcElement',
          'vfcGroup',
          varname,
          config.groupLabel,
          buttons,
          settings,
          fieldContainer
        );
      },

      /**
       * Themes a field container.
       *
       * @param {string} varname
       * @param {object} config
       * @param {object} displaySettings
       * @param {object} availableViewModes
       * @returns {string}
       */
      vfcField: function(varname, config, displaySettings, availableViewModes) {
        // Prepare field specific settings.
        if (availableViewModes.individual !== undefined) {
          var fieldSettings = {};

          // We need to break javascript references at multiple levels to
          // make it work flawlessly.
          var common_viewmodes = $.extend({}, availableViewModes.common);

          for (var id in availableViewModes.individual) {
            // Another stage of de-referencing.
            var individual_viewmodes = $.extend({}, availableViewModes.individual[id].view_modes);

            // Finally, the resulting object dereferenced...
            var combined_viewmodes = $.extend({}, common_viewmodes);
            $.extend(combined_viewmodes, individual_viewmodes);

            var settingElement = {
              label: availableViewModes.individual[id].label,
              type: 'select',
              options: combined_viewmodes
            };
            fieldSettings[id] = settingElement;
          }
        }
        else {
          var fieldSettings = {
            viewmode: {
              label: Drupal.t('View mode'),
              type: 'select',
              options: availableViewModes.common
            }
          };
        }

        // Get a complete settings object prepared for inclusion.
        var settings = Drupal.vfc.prepareElementSettings(fieldSettings, displaySettings, config);

        // Buttons for the element header.
        var buttons = [
          Drupal.theme('vfcButton', 'showHideContent', '&#9650;')
        ];

        // Theme the element and return it.
        return Drupal.theme(
          'vfcElement',
          config.type,
          varname,
          config.label,
          buttons,
          settings
        );
      }
    }
  }

  /**
   * Startup functions.
   */
  Drupal.behaviors.virtual_field_collection = {
    attach: function (context, settings) {
      // Extend Drupal with our own js theme hooks.
      $.extend(Drupal.theme.prototype, Drupal.vfc.theme);

      // initialize the interface
      $('textarea.vfc_serialized_data').once(function(index, field){
        Drupal.vfc.initInterface(field);
      });
    }
  }

}(jQuery));