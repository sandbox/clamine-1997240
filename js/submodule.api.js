/**
 * @file
 * This file is intended to be a blueprint for new sub-modules on how to
 * integrate properly with the virtual field collection interface api.
 *
 * Each sub-module has at least to implement the functions described here,
 * because they will get called by the api. Missing implementations will
 * result in errors thrown and remaining the sub-module useless!
 *
 * Also, make sure to use a correct name spacing for your implementation!
 */

(function ($) {
  // Be sure to change MODULENAME to your actual module's name.
  Drupal.behaviors.MODULENAME = {
    attach: function (context, settings) {
      jQuery.extend(Drupal.vfc.fieldapi, {

        // FIELDTYPE has to be *exactly* the same as the machine name of your
        // field type(s) provided. Please make *absolutely* sure your field
        // type does *not* collide with any other field types existing for vfc!
        FIELDTYPE: {

          /**
           * vfc callback checkConfiguration() - MANDATORY
           *
           * This callback is fired on the admin structure page for a
           * virtual field collection upon the event "safe configuration".
           * It lets you validate the settings a user made to an actual
           * implementation of your field type.
           *
           * Note: Please be kind to your users! Provide your error messages
           * wrapped by the Drupal.t function and deliver a localization
           * file with the needed string translations with your module!
           *
           * @param field {object}
           *   The actual settings made to the field instance.
           *
           * @param definition {object}
           *   The field definition as defined by your module.
           *
           * @param $DOMelem {HTMLElement}
           *   The jQuery DOM element of the instance.
           *
           * @returns {boolean|Array}
           *   VFC expects TRUE for a successful validation of the field
           *   instance configuration or an array of error messages to be
           *   displayed.
           */
          checkConfiguration: function(field, definition, $DOMelem) {
            return true || [Drupal.t('Error message(s)'), Drupal.t('...')];
          },

          /**
           * vfc callback getFormElement() - MANDATORY
           *
           * This callback is executed when virtual field collection is actually
           * creating an instance of your field type. VFC expects you to deliver
           * all the html markup for your field(s).
           *
           * Note: If you turn your field into an autocomplete field by calling
           * "Drupal.vfc.attachAutocomplete" (see callback description "afterBuild"
           * for more details), the "real value" of your field has to be stored as
           * a html attribute "data-value"! The "normal" value of the field is just
           * used as a label for the user. Also: *You* are in charge of providing a
           * proper label! Implement hook_virtualfield_prebuild() in your .module file,
           * which will receive the stored data as well as the field instance
           * configuration in order to add the needed bits of information to
           * Drupal.settings.
           *
           * @param varname {string}
           *   The variable name that was assigned to this field. Make sure that
           *   you *NOT* use a "name" html attribute on your input elements (your
           *   fields just don't need it, as vfc handles data storage for you.
           *   Your sub-module is required to provide a callback "getFieldValue"
           *   to retrieve the data to store.
           *
           *   Backdrop: VFC utilizes fields that are not known to the Drupal core
           *   FAPI framework. If these fields are sent to the server upon form
           *   submission, they will be simply ignored, and so there is absolutely
           *   no benefit in sending them. On the other hand, form input elements
           *   without a "name" attribute are ignored by the user's browser upon
           *   form submission and their values will not be included in the request.
           *   This helps save bandwidth and minimizes the request overhead.
           *
           * @param config {object}
           *   The configuration object for your field.
           *   Field label, the variable name and the required flag are located on
           *   the top level of this object. All the custom settings your field
           *   type does provide are to be found under config.settings.
           *
           * @param data {*}
           *   Depending on the structure of your field, this is the data for your
           *   input element(s). It's just in the same format you provide with the
           *   return value of the vfc callback "getFieldValue".
           *
           *   As the value of the data argument is in just the same format than
           *   the data your sub-module provided with it's callback "getFieldValue",
           *   you have to make sure that all html entities are escaped properly
           *   if you insert it as a "value" attribute to your field's markup.
           *
           *   E.g. if the argument value is: <Life is "beautiful"> you have to escape
           *   it like &lt;Life is &quot;beautiful&quot;&gt; - Virtual Field Collection
           *   provides a convenient way to do this:
           *   escaped = Drupal.vfc.htmlEscapeString(string)
           *
           *
           * @returns {string}
           *   The html markup for your input element(s). It is important to notice
           *   that you will receive the same DOM elements as parameters to all
           *   vfc callbacks receiving the $input argument! So please take care of
           *   that when wrapping your form elements with additional markup.
           */
          getFormElement: function(varname, config, data) {
            return '<input....';
          },

          /**
           * vfc callback afterBuild() - MANDATORY
           *
           * This callback is executed after actually placing your form
           * element(s) in the DOM structure of the entity edit form. It gives
           * you the possibility to bind your inputs to events like "click",
           * "change" etc in order to fire actions if desired.
           *
           * If you want to use the autocomplete feature, this is the place to
           * do it. Note: Only input elements of type "text" can be autocomplete
           * fields!
           *
           * To attach an autocomplete behaviour, you have to provide at least
           * the callback function. Possible arguments are a filter value and a
           * realm, which may be handy to distinct between several different
           * autocompletes (e.g. reference has realms "taxonomy", "user" and "node".
           *
           * Your callback has to provide an array of objects with the following
           * structure:
           * array(
           *   stdClass(
           *     value: 'your "real" value',
           *     label: 'the label to display',
           *     category: 'category to sort item in' (optional)
           *   )
           * )
           *
           * @param $input {HTMLElement}
           *   The DOM structure as delivered by your callback "getFormElement",
           *   but already wrapped in a jQuery object for your convenience.
           *
           * @param config
           *   The configuration object for your field.
           *   Field label, the variable name and the required flag are located on
           *   the top level of this object. All the custom settings your field
           *   type does provide are to be found under config.settings.
           *
           * @returns none
           */
          afterBuild: function($input, config) {
            // Attach a click event handler.
            $input.bind('click', $function() {});

            // Attach autocomplete behaviour.
            Drupal.vfc.attachAutoComplete(
              // MUST to be a *SINGLE* text field!
              $input,
              'my_callback_function'
              // 'my filter value' (optional argument)
              // my realm (optional argument)
            );
          },

          /**
           * vfc callback checkFieldEmpty() - MANDATORY
           *
           * This callback is executed prior to form validation on entity
           * edit forms. VFC expects you to state if any inputs were made
           * to your field type. Empty elements will be removed from the
           * DOM tree prior to validation in order to ignore empty fields.
           * Due to the fact that VFC cannot know what html structure you
           * are providing with your field type it's your due to determine
           * this needed piece of information.
           *
           * Note: don't worry about empty, but required fields. VFC takes
           * care of this itself. Also, empty fields will be "respawned" to
           * the entity edit form after a failed validation automatically.
           *
           * Note: If you turned your field into an autocomplete field by calling
           * "Drupal.vfc.attachAutocomplete" (see callback description "afterBuild"
           * for more details), the "real value" of your field is stored in the
           * html attribute "data-value", NOT the value of the field itself (this
           * is only the label for the value)!
           *
           * @param $input {HTMLElement}
           *   The DOM structure as delivered by your callback "getFormElement",
           *   but already wrapped in a jQuery object for your convenience.
           *
           * @param config
           *   The configuration object for your field.
           *   Field label, the variable name and the required flag are located on
           *   the top level of this object. All the custom settings your field
           *   type does provide are to be found under config.settings.
           *
           * @returns {boolean}
           */
          checkFieldEmpty: function($input, config) {
            return true || false;
          },

          /**
           * vfc callback validateField() - MANDATORY
           *
           * This callback is executed within the form validation run of VFC.
           * As stated on the api information to the "checkFieldEmpty" callback,
           * only field instances with actual user inputs are to be validated
           * by your module, because empty elements were completely removed. So
           * if this callback is executed, there has to be any user input to
           * actually validate - you named it! ;-)
           *
           * Due to the fact that VFC cannot know what html structure you
           * are providing with your field type nor what data structure your module
           * is about to provide it's your due to determine this needed piece of
           * information.
           *
           * Note: If you turned your field into an autocomplete field by calling
           * "Drupal.vfc.attachAutocomplete" (see callback description "afterBuild"
           * for more details), the "real value" of your field is stored in the
           * html attribute "data-value", NOT the value of the field itself (this
           * is only the label for the value)!
           *
           * Note: Please be kind to your users! Provide your error messages
           * wrapped by the Drupal.t function and deliver a localization
           * file with the needed string translations with your module!
           *
           * @param $input {HTMLElement}
           *   The DOM structure as delivered by your callback "getFormElement",
           *   but already wrapped in a jQuery object for your convenience.
           *
           * @param config
           *   The configuration object for your field.
           *   Field label, the variable name and the required flag are located on
           *   the top level of this object. All the custom settings your field
           *   type does provide are to be found under config.settings.
           *
           * @returns {boolean|Array}
           *   VFC expects TRUE for a successful validation of the field
           *   instance configuration or an array of error messages to be
           *   displayed.
           */
          validateField: function($input, config) {
            return true || [Drupal.t('Error message(s)'), Drupal.t('...')];
          },

          /**
           * vfc callback prepareSerialization() - OPTIONAL
           *
           * As each virtual field will get serialized before it is saved
           * to the site's database, it is perhaps necessary to do some
           * adjustments to the field's value before this happens. This
           * callback is called right before the callback "getFieldValue"
           * is called, on the same instance of the field's form element(s).
           *
           * @param $input {HTMLElement}
           *   The DOM structure as delivered by your callback "getFormElement",
           *   but already wrapped in a jQuery object for your convenience.
           *
           * @param config
           *   The configuration object for your field.
           *   Field label, the variable name and the required flag are located on
           *   the top level of this object. All the custom settings your field
           *   type does provide are to be found under config.settings.
           *
           * @returns none
           */
          prepareSerialization: function($input, config) {

          },

          /**
           * vfc callback getFieldValue() - MANDATORY
           *
           * This callback is executed within the validation run on entity
           * edit forms. As stated twice above, it is only executed on
           * instances of your fields which do actually contain data as
           * determined by your module.
           *
           * Due to the fact that VFC cannot know what html structure you
           * are providing with your field type nor what data structure your module
           * is about to provide it's your due to determine this needed piece of
           * information.
           *
           * Note: If you turned your field into an autocomplete field by calling
           * "Drupal.vfc.attachAutocomplete" (see callback description "afterBuild"
           * for more details), the "real value" of your field is stored in the
           * html attribute "data-value", NOT the value of the field itself (this
           * is only the label for the value)!
           *
           * @param $input {HTMLElement}
           *   The DOM structure as delivered by your callback "getFormElement",
           *   but already wrapped in a jQuery object for your convenience.
           *
           * @param config
           *   The configuration object for your field.
           *   Field label, the variable name and the required flag are located on
           *   the top level of this object. All the custom settings your field
           *   type does provide are to be found under config.settings.
           *
           * @returns {*}
           *   Feel free to provide whatever you like, as long as it *can* be
           *   serialized. Expect the same data structure to get as an argument
           *   to your implementation of vfc's callback "getFormElement". As a
           *   general rule it is a good idea to keep your data structure as simple
           *   and brief as it can get. The more complex or verbose your data
           *   structure is, the larger is the amount of data stored to the site's
           *   database and will possibly also have an impact on the performance
           *   of the javascript interface. E.g. if your module returns a data object,
           *   try to keep the property names as short as possible (as long as *you*
           *   can profundly determine what data hides behind them) because these
           *   property names will inflate the serialized data a lot and will have
           *   no real benefit for any user. Also, verbose property names will possibly
           *   have a negative impact on performance of the interface due to memory
           *   usage and de-serialization performance.
           */
          getFieldValue: function($input, config) {
            return 'What ever you like to be stored to the database...';
          }
        }

      });
    }
  }
}(jQuery));
