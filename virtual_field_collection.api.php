<?php

/**
 * @file
 * Defines hook functions for the virtual field collection module for use in submodules
 * in order to provide field types.
 */

/**
 * hook_virtualfield_info() - MANDATORY
 *
 * This hook is invoked when virtual field collection builds up
 * the field administration widget. It returns an in-detail view
 * on the field types your module provides including their
 * individual settings.
 *
 * Setting types include:
 * text - rendered as a plain text field
 * int - rendered as a text field, but forced to convert to a number
 * bool - rendered as a single checkbox
 * select - rendered as a select list (1 choice out of N options)
 * set - rendered as a group of checkboxes (x choices out of N options)
 *
 * Your module is required to implement Drupal's hook_library in order
 * to provide the javascript interface file to virtual field collection.
 * @see hook_library()
 *
 * @param $fieldname (optional)
 * @return array
 */
function hook_virtualfield_info($fieldname = NULL) {
  $fields = array(
    'fieldmachinename' => array(
      'group'    => 'Label to use for the optgroup label',
      'label'       => t('Human readable field type label'),
      'description' => t('Short description of the field type'),
      'settings' => array(
        'settingvariable' => array(
          'label'   => t('Human readable name of the setting'),
          'type'    => 'text|int|bool|select|set',
          'options' => array(
            'option value' => t('Option label'),
          ),
          'default' => 'Default value for this setting (in case of set or select: the data key)',
        ),
      ),
      'jslibrary' => 'machine name of the javascript library to add',
    ),
  );

  return !empty($fieldname) ? $fields[$fieldname] : $fields;
}

/**
 * hook_virtualfield_register - OPTIONAL
 *
 * This hook gets invoked everytime a field of the provided type gets registered
 * in a virtual field collection. You can take preparation steps for the later
 * use of this field, e.g. create directories for files or similar tasks.
 *
 * @param $varname
 * @param $config
 * @return none
 */
function hook_virtualfield_register($varname, $config) {
  // do some crazy stuff here if you like
}

/**
 * hook_virtualfield_load() - MANDATORY
 *
 * This hook is invoked every time a field of this module is loaded. It is
 * expected to return the processed data for the field instance. E.g. if you
 * store an entity id as your field's value, this is the place to provide
 * the entity itself.
 *
 * @param $config
 * @param $value
 * @return mixed
 */
function hook_virtualfield_load($config, $value) {
  return 'Your processed data';
}

/**
 * hook_virtualfield_get_viewmodes() - MANDATORY
 *
 * This hook is called when the display settings of a virtual field
 * collection instance are configured. VFC expects the module to
 * return an array with information about possible view modes for
 * this field.
 *
 * @param $config
 * @return array
 */
function hook_virtualfield_get_viewmodes($config) {
  return array(
    'common' => array(
      'machine_name' => 'Human viewmode name'
    ),
    'individual' => array(
      'machine_name' => 'Human viewmode name'
    ),
  );
}

/**
 * hook_virtualfield_view() - MANDATORY
 *
 * This hook gets called when the actual output of this field is requested.
 * VFC expects the module to return a render array ready for rendering.
 *
 * @param $config
 * @param $displaysettings
 * @param $value
 * @return array
 */
function hook_virtualfield_view($config, $displaysettings, $value) {
  return array(
    // drupal standard render array
  );
}

/**
 * hook_virtualfield_autocomplete() - OPTIONAL
 *
 * This function gets called when you apply the vfc autocomplete feature
 * on your field input. As an argument to the vfc api autocomplete
 * feature you have to provide this exact function name.
 *
 * It returns an array of objects containing the autocomplete suggestions.
 *
 * @param $string
 * @param $filter
 * @param $realm
 * @return array
 */
function hook_virtualfield_autocomplete($string, $filter, $realm) {
  return array(
    (object) array(
      'value' => 'Real value for the field',
      'label' => 'Display value',
      'category' => 'Category to sort this suggestion in',
    ),
  );
}

/**
 * hook_virtualfield_prebuild() - OPTIONAL
 *
 * This hook is invoked during form building on a form with a virtual field
 * collection. It enables you to load additional data for your field's
 * form element, which has to be injected as a setting to the page javascript.
 *
 * @see drupal_add_js()
 *
 * @param $varname
 * @param $config
 * @param $value
 * @return none
 */
function hook_virtualfield_prebuild($varname, $config, $value) {
  $settings['your_setting_entry'] = array(
    'your_key' => 'your settings...',
  );

  // Insert the javascript settings object.
  drupal_add_js($settings, 'setting');
}

/**
 * hook_virtualfield_presave() - OPTIONAL
 *
 * This hook is called right before the virtual field collection value is
 * written to the database and allows for altering the virtual field's value.
 * If this hook is implemented and actually alters the virtual field's data
 * be sure to expect the altered value when editing the field.
 *
 * @param $config
 * @param $value
 * @param $entity_type
 * @param $entity
 */
function hook_virtualfield_presave($config, &$value, $entity_type, $entity) {

}
